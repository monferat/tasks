package controller;

/**
 * GeekHub. Lesson 9 
 * Task 9.1 (Make web-project using Spring & Spring MVC)
 *  
 */  

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class MyController {

	@RequestMapping("/session")
	public ModelAndView newDoGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException{

		ModelAndView model = new ModelAndView("JFile");

		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		HttpSession session = request.getSession();

		if (action == null) {
			return model;
		}

		if (action.equals("add")) {	
			if(name != null && value !=null)
			   session.setAttribute(name, value);
			return model;
		}

		if (action.equals("remove")) {
			if (name != null)
				session.removeAttribute(name);
			return model;
		}
		
		request.getSession();
		return model;
	}

}