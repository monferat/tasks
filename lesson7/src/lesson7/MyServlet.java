package lesson7;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MyServlet
 */
public class MyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String message = request.getParameter("message");
		String action = request.getParameter("action");
		String scope = request.getParameter("scope");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		HttpSession session = request.getSession();
		ServletContext context = request.getServletContext();
		boolean invalidateStatus = false;
		response.setContentType("text/html");
		if (message != null) {
			response.getWriter().println("<hr><br>Message:<br>" + message + "<br>");
		}

		if ("session".equals(scope)) {
			if ("invalidate".equals(action)) {
				session.invalidate();
				invalidateStatus = true;
			} else if (name != null) {
				if ("add".equals(action) || "update".equals(action) && value != null) {
					session.setAttribute(name, value);
				} else if (action.equals("remove")) {
					session.removeAttribute(name);
				}
			}
		} else if ("app".equals(scope)) {
			if (name != null) {
				if (context.equals("add") || context.equals("update") && value != null) {
					context.setAttribute(name, value);
				} else if (context.equals("remove")) {
					context.removeAttribute(name);
				}
			}
		}

		printSessionParameters(response, session, invalidateStatus);
		printContextParameters(response, context);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * Method prints all session parameters
	 * 
	 * @param response
	 * @param session
	 * @param invalidateStatus
	 * @throws IOException
	 */
	public void printSessionParameters(HttpServletResponse response,
			HttpSession session, boolean invalidateStatus) throws IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		if (invalidateStatus == false) {
			out.println("<hr><br>Session:" + "<br>");
			@SuppressWarnings("rawtypes")
			Enumeration Session = session.getAttributeNames();
			while (Session.hasMoreElements()) {
				String key = (String) Session.nextElement();
				out.println(key + ": " + session.getAttribute(key) + "<br>");
			}
		}
	}

	/**
	 * Method prints all servlet context parameters
	 * 
	 * @param response
	 * @param context
	 * @throws IOException
	 */
	public void printContextParameters(HttpServletResponse response,
			ServletContext context) throws IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("<hr><br>Context:" + "<br>");
		@SuppressWarnings("rawtypes")
		Enumeration Context = context.getAttributeNames();
		while (Context.hasMoreElements()) {
			String key = (String) Context.nextElement();
			out.println(key + ": " + context.getAttribute(key) + "<br>");
		}
		out.println("<hr>");
	}

}
