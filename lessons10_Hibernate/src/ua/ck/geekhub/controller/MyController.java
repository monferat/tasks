package ua.ck.geekhub.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ua.ck.geekhub.entity.Group;
import ua.ck.geekhub.entity.User;
import ua.ck.geekhub.manager.UserManager;

@Controller
public class MyController {
	
	/** 
	 *  Add new user
	*/
	@RequestMapping("/add")
	public ModelAndView addUser(HttpServletRequest request,
			HttpServletResponse response) {

		List<Group> groups = null;
		Integer groupId = null;
		Group gr = null;
		ModelAndView m = new ModelAndView("user");

		if (request.getParameter("action") == null) {
			groups = UserManager.getInstance().getGroups();
			m.addObject("groups", groups);
			return m;
		}
		
		User user = new User();

		if (request.getParameter("groupId") != null) {
			groupId = Integer.parseInt(request.getParameter("groupId"));
			gr = UserManager.getInstance().getGroupById(groupId);
		}
		
		user.setUserLogin(request.getParameter("userName"));
		user.setEmail(request.getParameter("email"));
		user.setEmail(request.getParameter("password"));
		user.setGroup(gr);
		UserManager.getInstance().addUser(user);
		return m;
	}
}