package ua.ck.geekhub.entity;


import java.util.ArrayList;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="GROUP")
public class Group {
        @Id
        @Column(name="GROUP_ID")
        @GeneratedValue
        private Integer id;
        
        @Column
        private String groupName;
        
        @OneToMany(mappedBy =  "group")
        private ArrayList<User> users;
        
        
        public Integer getId() {
                return id;
        }

        public void setId(Integer id) {
                this.id = id;
        }
        
        public String getGroupName() {
                return groupName;
        }

        public void setGroupName(String groupName) {
                this.groupName = groupName;
        }
        
        public ArrayList<User> getUsers(){
                return users;
        }
        public void setUsers(ArrayList<User> users){
                this.users = users;
        }
}
