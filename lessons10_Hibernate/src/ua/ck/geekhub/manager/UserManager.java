package ua.ck.geekhub.manager;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import ua.ck.geekhub.HibernateUtil;
import ua.ck.geekhub.entity.User;
import ua.ck.geekhub.entity.Group;

public class UserManager {

	private static UserManager instance;

	private UserManager() {
	}

	public static UserManager getInstance() {
		if (instance == null)
			instance = new UserManager();
		return instance;
	}

	public void addUser(User user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
		session.close();
	}

	public void addGroup(Group group) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.save(group);
		session.getTransaction().commit();
		session.close();
	}

	public void deleteUser(User user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.delete(user);
		session.getTransaction().commit();
		session.close();
	}

	public void deleteGroup(Group group) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		session.delete(group);
		session.getTransaction().commit();
		session.close();
	}

	@SuppressWarnings("unchecked")
	public List<Group> getGroups() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		List<Group> cr = session.createCriteria(Group.class).list();
		session.getTransaction().commit();
		session.close();
		return cr;
	}

	@SuppressWarnings("unchecked")
	public List<User> getUsersByGroup(Group group) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		session.beginTransaction();
		Criteria cr = session.createCriteria(User.class).addOrder(Order.asc("group"));
		session.getTransaction().commit();
		List<User> ur = cr.list();
		session.close();
		return ur;
	}
	
	public Group getGroupById(Integer id) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        Group gr = (Group)session.createCriteria(Group.class).add(Restrictions.eq("id", id)).list().get(0);
        session.getTransaction().commit();
        session.close();
        return gr;
}

}
