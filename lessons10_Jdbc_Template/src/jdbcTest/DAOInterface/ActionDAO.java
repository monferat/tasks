package jdbcTest.DAOInterface;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import jdbcTest.model.Animal;

public class ActionDAO implements IDao {
	private DataSource dataSource;

	public void setDataSource(DataSource ds) {
		dataSource = ds;
	}

	/**
	 * Create pet and insert to database 
	 */
	public void addToDB(Animal pet) {
		JdbcTemplate insert = new JdbcTemplate(dataSource);
		insert.update(
				"INSERT INTO PETS (SPECIES, NAME, AGE, COLOR) VALUES(?,?,?,?)",
				new Object[] { pet.getSpecies(), pet.getName(), pet.getAge(), pet.getColor() });
	}
	
	/**
	 * Delete all pets from table 
	 */
	public void deleteAll() {
		JdbcTemplate delete = new JdbcTemplate(dataSource);
		delete.update("DELETE from PETS");
	}

	/**
	 * Delete some pets from table 
	 */
	public void delete(Animal pet) {
		JdbcTemplate delete = new JdbcTemplate(dataSource);
		delete.update("DELETE from PETS where SPECIES= ? AND NAME = ? AND AGE = ? AND COLOR = ?",
				new Object[] { pet.getSpecies(), pet.getName(), pet.getAge(), pet.getColor() });
	}
	
	/**
	 * Delete some pets by ID 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public void deleteById(int id) {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		String sql = "DELETE * FROM PETS WHERE PETS_ID = ?";
		jdbc.queryForObject(sql, new Object[] { id }, new BeanPropertyRowMapper(Animal.class));
	}

	/**
	 * Select all pets from table 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<Animal> selectAll() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		String sql = "SELECT * FROM PETS";
		List<Animal> pets = jdbc.query(sql, new BeanPropertyRowMapper(Animal.class));
		return pets;
	}

	/**
	 * Select pet from table with some id 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Animal findById(int id) {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		String sql = "SELECT * FROM PETS WHERE PETS_ID = ?";
		Animal pet = jdbc.queryForObject(sql, new Object[] { id },
				new BeanPropertyRowMapper(Animal.class));

		return pet;
	}

}