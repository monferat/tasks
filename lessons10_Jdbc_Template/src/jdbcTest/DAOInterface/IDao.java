package jdbcTest.DAOInterface;

import java.util.List;

import javax.sql.DataSource;
import jdbcTest.model.Animal;

/**
 * Create the Data Access Object (DAO)Interface
 */
public interface IDao {

	void setDataSource(DataSource ds);
	void addToDB(Animal pet);
	void deleteAll();
	void delete(Animal pet);
	List<Animal> selectAll();
	Animal findById(int id);

}