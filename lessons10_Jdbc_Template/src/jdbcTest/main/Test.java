package jdbcTest.main;

import java.util.List;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import jdbcTest.DAOInterface.ActionDAO;
import jdbcTest.model.Animal;

public final class Test {

	public static void main(String[] args) {
		ActionDAO dao = new ActionDAO();

		// Initialize the datasource
		DriverManagerDataSource dataSource = new DriverManagerDataSource();

		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost:3306/animals");
		dataSource.setUsername("root");
		dataSource.setPassword("polina");

		// Inject the datasource into the dao
		dao.setDataSource(dataSource);
		
		Animal pet1 = new Animal("fish", "Larsy", 2, "gold"); 
		dao.addToDB(pet1);

		System.out.println("Now select and list all pets:");
		List<Animal> list = dao.selectAll();

		for (Animal pet : list) {
			System.out.printf("%-7s %s\n",pet.getSpecies(),pet.getName());
		}

		System.out.println("Now select and list all pets with have the id = 3");
		Animal pet = dao.findById(3);

		System.out.println(pet.getSpecies() + " " + pet.getName());
		
		//dao.delete(pet1);

	}
}
