package jdbcTest.model;

/**
 * Create the following domain model.
 *
 */
public class Animal {

	int pets_id;
	private String species;
	private String name;
	private int age;
	private String color;
	
	/**Constructor I*/
	public Animal(){
	
	}
	
	/**Constructor II*/
	public Animal(String species, String name, int age,String color){
		this.species = species;
		this.name = name;
		this.age = age;
		this.color = color;
	}
	
	public int getId() {
		return pets_id;
	}

	public void setId(int pets_id) {
		this.pets_id = pets_id;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

}