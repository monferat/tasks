<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<table border="1">
    <tr>
        <th>Title</th>
        <th>Description</th>
        <th>Owner</th>
        <th>Status</th>
        <th>Priority</th>
        <th></th>
    </tr>
    <c:forEach items="${tickets}" var="ticket">
        <tr>
            <td>${ticket.title}</td>
            <td>${ticket.description}</td>
            <td>${ticket.owner}</td>
            <td>${ticket.status}</td>
            <td>${ticket.priority}</td>
            <td><a href="/Bugtracker/loadTicket.html?id=${ticket.id}">Edit</a></td>
            <td style="color: red;"><a href="/Bugtracker/deleteTicket.html?id=${ticket.id}">Delete</a></td>
        </tr>
    </c:forEach>
</table>

<br><a href="/Bugtracker/loadTicket.html">Create New Ticket</a>
<br><a href="/Bugtracker/index.html">Main Page</a>