<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>CSS3 Form Table</title> 
<style type="text/css">
	.CSSTable {
		margin:0px;
		padding:0px;
		width:70%;
		box-shadow: 10px 10px 5px #888888;
		border:1px solid #000000;
	
		-moz-border-radius-bottomleft:0px;
		-webkit-border-bottom-left-radius:0px;
		border-bottom-left-radius:0px;
	
		-moz-border-radius-bottomright:0px;
		-webkit-border-bottom-right-radius:0px;
		border-bottom-right-radius:0px;
	
		-moz-border-radius-topright:0px;
		-webkit-border-top-right-radius:0px;
		border-top-right-radius:0px;
	
		-moz-border-radius-topleft:0px;
		-webkit-border-top-left-radius:0px;
		border-top-left-radius:0px;
	
	}
	.CSSTable table{
		width:100%;
		height:10%;
		margin:0px;
		padding:0px;
	}
	.CSSTable tr:last-child td:last-child {
		-moz-border-radius-bottomright:0px;
		-webkit-border-bottom-right-radius:0px;
		border-bottom-right-radius:0px;
	}
	.CSSTable table tr:first-child td:first-child {
		-moz-border-radius-topleft:0px;
		-webkit-border-top-left-radius:0px;
		border-top-left-radius:0px;
	}
	
	.CSSTable table tr:first-child td:last-child {
		-moz-border-radius-topright:0px;
		-webkit-border-top-right-radius:0px;
		border-top-right-radius:0px;
	
	}.CSSTable tr:last-child td:first-child{
		-moz-border-radius-bottomleft:0px;
		-webkit-border-bottom-left-radius:0px;
		border-bottom-left-radius:0px;
	
	}.CSSTable tr:hover td{
		background-color:#ffffff;
	}
	
	.CSSTable td{
		vertical-align:middle;
		background:-o-linear-gradient(bottom, #aad4ff 5%, #ffffff 100%);
		background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #aad4ff), color-stop(1, #ffffff) ); 
		background:-moz-linear-gradient( center top, #aad4ff 5%, #ffffff 100% );
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#aad4ff", endColorstr="#ffffff");
		background: -o-linear-gradient(top,#aad4ff,ffffff);
		background-color:#aad4ff;
		border:1px solid #000000;
		border-width:0px 1px 1px 0px;
		text-align:center;
		padding:10px;
		font-size:14px;
		font-family:Arial;
		font-weight:normal;
		color:#000000;
	
	}.CSSTable tr:last-child td{
		border-width:0px 1px 0px 0px;
	}.CSSTable tr td:last-child{
		border-width:0px 0px 1px 0px;
	}.CSSTable tr:last-child td:last-child{
		border-width:0px 0px 0px 0px;
	}
	
	.CSSTable tr:first-child td{
		background:-o-linear-gradient(bottom, #005fbf 5%, #003f7f 100%);
		background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #005fbf), color-stop(1, #003f7f) );
		background:-moz-linear-gradient( center top, #005fbf 5%, #003f7f 100% );
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#005fbf", endColorstr="#003f7f");
		background: -o-linear-gradient(top,#005fbf,003f7f);
		background-color:#005fbf;
		border:0px solid #000000;
		text-align:center;
		border-width:0px 0px 1px 1px;
		font-size:16px;
		font-family:Arial;
		font-weight:bold;
		color:#ffffff;
	}
	
	.CSSTable tr:first-child:hover td{
		background:-o-linear-gradient(bottom, #005fbf 5%, #003f7f 100%);
		background:-webkit-gradient( linear, left top, left bottom, color-stop(0.05, #005fbf), color-stop(1, #003f7f) );
		background:-moz-linear-gradient( center top, #005fbf 5%, #003f7f 100% );
		filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#005fbf", endColorstr="#003f7f");
		background: -o-linear-gradient(top,#005fbf,003f7f);
		background-color:#005fbf;
	}
	
	.CSSTable tr:first-child td:first-child{
		border-width:0px 0px 1px 0px;
	
	}
	
	.CSSTable tr:first-child td:last-child{
		border-width:0px 0px 1px 1px;
	
	}
</style>
</head>
<body>
	<div class="CSSTable" >
		<table border="1">
		    <tr>
		        <th>Login</th>
		        <th>Password</th>
		        <th>Name</th>
		        <th></th>
		        <th></th>
		    </tr>
		
		<c:forEach items="${users}" var="user">
		    <tr>
		        <td>${user.login}</td>
		        <td>${user.password}</td>
		        <td>${user.name}</td>
		        <td><a href="/Bugtracker_Hibernate/loadUser.html?id=${user.id}">Edit</a></td>
		        <td style="color: red;"><a href="/Bugtracker_Hibernate/deleteUser.html?id=${user.id}">Delete</a></td>
		
		    </tr>
		</c:forEach>
		</table>
	</div>
	<p><a href="/Bugtracker_Hibernate/loadUser.html">Create New User</a></p>
	<p><a href="/Bugtracker_Hibernate/index.html">Main Page</a></p>
</body>
</html>
