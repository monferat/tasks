<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
	<title>CSS3 Form</title> 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
	<style>
		.error {
			margin-left: 3px;
			color: #FF0000;
		}
		body { 
			padding:50px 120px;
			font:13px/150% Verdana, Tahoma, sans-serif;
			}
		.form {
			margin:0 auto;
			width:600px;
			background: -webkit-gradient(linear, left top, left 25, from(#FFFFFF), color-stop(4%, #D1DDDE), to(#E0EEEE));
			border-color:#000;  
			padding:44px;
			box-shadow: 1px 1px 0 0 #949494;
			-moz-box-shadow: 1px 1px 0 0 #949494;
			-webkit-box-shadow: 1px 1px 0 0 #949494;
			border-radius: 18px 18px;
			-moz-border-radius: 18px 18px;
			-webkit-border-radius: 18px 18px;
			}
		input, textarea {
			padding: 9px;
			border: solid 1px #E5E5E5;
			outline: 1px;
			font: normal 13px/100% Verdana, Tahoma, sans-serif;
			width: 200px;
			background: #FFFFFF url('bg_form.png') left top repeat-x;
			background: -webkit-gradient(linear, left top, left 25, from(#FFFFFF), color-stop(4%, #EEEEEE), to(#FFFFFF));
			box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;
			-moz-box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;
			-webkit-box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;
			background-color: #6CF;
			background-position: top;
			}  
		input:hover, textarea:hover,  
		input:focus, textarea:focus {  
			border-color: #C9C9C9;  
			-webkit-box-shadow: rgba(0, 0, 0, 0.15) 1px 5px 8px;  
			}
		.form label {  
			margin-left: 0px;  
			color: #3399FF;  
			}  
		.submit input {  
			width: auto;  
			padding: 9px 15px;  
			background: #6AA798;  
			border: 0;  
			font-size: 14px;  
			color: #FFFFFF;  
			-moz-border-radius: 5px;  
			-webkit-border-radius: 5px;  
			} 
			h1 {
      		text-shadow: 0 0 10px #FFF,
            		     0 0 20px #FFF,
                   		 0 0 30px #FFF;
            font-size: 18px;                    	
			}	
	</style>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
		<script>
			$(document).ready(function(){
				$("#form").validate({
					rules:{
						login:{
							required: true,
							minlength: 3
						},
						password:{
							required: true,
							minlength: 6
						},
						name: 'required'
					},
				    messages: {
				    	login: {
				    		required: " Login required",
				    		minlength: " Your Login must be 3 characters at least"
				   		},
				   		name: " Please specify your name",
				   		password:{
				   			required: " Password required",
				   			minlength: " Password must be 6 characters or more"
				   		}
					}
				});
			});
		</script>
	</head>
	<body>
		<form class="form" id="form" action="/Bugtracker_Hibernate/saveUser.html">
			<div><h1>Create user:</h1><br></div>
			<p class="login"> <label for="login">Login</label></p>
			<p class="login"> <input type="text" value="${user.login}" name="login" id="login" /></p>
			<p class="login"><span class="password">
		    <label for="password3">Password</label>
			</span></p>  
			<p class="password">  
				<input type="text" value="${user.password}" name="password" id="password" />
		    </p>
			<p class="password"><span class="name">
			  <label for="name3">Name</label>
            </span> </p>  
			<p class="name">  
				<input type="text" value="${user.name}" name="name" id="name" />
		  	</p>	    
		    <input type="hidden" name="id" value="${user.id}">
		    <p class="submit">  
				<input type="submit" value="Save" />  
		  	</p>  
		</form>
	</body>	
</html>