<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
	<title>CSS3 Form</title> 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
	<style>
		.error {
			margin-left: 3px;
			color: #FF0000;
			}
		body { 
			padding:50px 120px;
			font:13px/150% Verdana, Tahoma, sans-serif;
			}
		.form {
			margin:0 auto;
			width:600px;
			background: -webkit-gradient(linear, left top, left 25, from(#FFFFFF), color-stop(4%, #D1DDDE), to(#E0EEEE));
			border-color:#000;  
			padding:44px;
			box-shadow: 1px 1px 0 0 #949494;
			-moz-box-shadow: 1px 1px 0 0 #949494;
			-webkit-box-shadow: 1px 1px 0 0 #949494;
			border-radius: 18px 18px;
			-moz-border-radius: 18px 18px;
			-webkit-border-radius: 18px 18px;
			}
		input, textarea {
			padding: 9px;
			border: solid 1px #E5E5E5;
			outline: 1px;
			font: normal 13px/100% Verdana, Tahoma, sans-serif;
			width: 200px;
			background: #FFFFFF url('bg_form.png') left top repeat-x;
			background: -webkit-gradient(linear, left top, left 25, from(#FFFFFF), color-stop(4%, #EEEEEE), to(#FFFFFF));
			box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;
			-moz-box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;
			-webkit-box-shadow: rgba(0,0,0, 0.1) 0px 0px 8px;
			background-color: #6CF;
			background-position: top;
			}  
		input:hover, textarea:hover,  
		input:focus, textarea:focus {  
			border-color: #C9C9C9;  
			-webkit-box-shadow: rgba(0, 0, 0, 0.15) 1px 5px 8px;  
			}
		.form label {  
			margin-left: 0px;  
			color: #3399FF;  
			}  
		.submit input {  
			width: auto;  
			padding: 9px 15px;  
			background: #6AA798;  
			border: 0;  
			font-size: 14px;  
			color: #FFFFFF;  
			-moz-border-radius: 5px;  
			-webkit-border-radius: 5px;  
			} 
		h1 {
      		text-shadow: 0 0 10px #FFF,
            		     0 0 20px #FFF,
                   		 0 0 30px #FFF;
            font-size:   18px;                    	
			}	
	</style>
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
		<script>
			$(document).ready(function(){
				$("#form").validate({
					rules:{
						title: {
							required: true,
							minlength: 4
						},
						description: {
							required: true,
							minlength: 6
						}
					},
				    messages: {
				    	title: {
				    		required: "Title required",
				    		minlength: "Your title must be 4 characters at least"
				   		},				   		
				   		description: {
				   			required: "Description required",
				   			minlength: "Description must be 6 characters at least"
				   		}
					}
				});
			});
		</script>
	</head>
	<body>
		<form class="form" id="form" action="/Bugtracker_Hibernate/saveTicket.html">
			<div><h1>Create ticket:</h1><br></div>
		    Title<br> <input type="text" value="${ticket.title}" name="title"><br>
		    Description<br> <input type="text" value="${ticket.description}" name="description"><br>
		    Status 
		    <select name="status">
		        <option value="ACTIVE">Active</option>
		        <option value="RESOLVED">Resolved</option>
		        <option value="TESTED">Tested</option>
		        <option value="PUSHED">Pushed</option>
		    </select><br>
		
		    Priority
		    <select name="priority">
		        <option value="LOW">Low</option>
		        <option value="NORMAL">Normal</option>
		        <option value="HIGH">High</option>
		    </select><br>
		
		    OwnerId
		    <select name="owner.id">
		    <c:forEach items="${users}" var="user">
		        <option value="${user.id}">${user.name} (${user.id})</option>
		    </c:forEach>
		    </select><br>
		    <input type="hidden" name="id" value="${ticket.id}">
		    <p class="submit">  
				<input type="submit" value="Save" />  
		  	</p>  
		</form>
	</body>
</html>