package lesson8;

/**
 * GeekHub. Lesson 8 
 * Task 8.1 (Make servlet using JSP & JSTL)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 10 Dec 2012.
 *  
 */

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class MyServlet
 */
public class MyServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MyServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		String action = request.getParameter("action");
		String name = request.getParameter("name");
		String value = request.getParameter("value");
		HttpSession session = request.getSession();
		
		if (name != null) {
			if ("remove".equals(action)) {
				session.removeAttribute(name);
			} else if (value != null) {
				session.setAttribute(name, value);
			}
		}
		request.getSession();
		String jsp = "JFile.jsp";
		request.getRequestDispatcher(jsp).forward(request, response);

	}

}
