<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Session</title>
<style type="text/css">
body,table,hr {
	color: black;
	background: #E6E6FA;
}
</style>
</head>
<body>
	<center>
		<h3>Session</h3>
		<form action="/lesson8/MyServlet">
			<table border=1>
				<tr>
					<th>Name:</th>
					<th>Value:</th>
					<th>Action</th>
				</tr>
				<c:set var="valuesSession" value="${sessionScope}"></c:set>
				<c:forEach var="s" items="${valuesSession}">
					<c:if test="${s.key != null && s.value != null}">
						<tr>
							<td>${s.key}</td>
							<td>${s.value}</td>
							<td><a href="MyServlet?action=remove&name=${s.key}">delete</a></td>
						</tr>
					</c:if>
				</c:forEach>
				<tr>
					<td><input type='text' name='name' value=''></td>
					<td><input type='text' name='value' value=''></td>
					<td><input type='submit' value='add'></td>
				</tr>
			</table>
		</form>
	</center>
</body>
</html>