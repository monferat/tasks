package car.interfaces;

public interface StatusAware {

	/** Prints status */
	public void printStatus();
	
}