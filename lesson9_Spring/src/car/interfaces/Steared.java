package car.interfaces;

import car.components.SteeringWheel;

public interface Steared {

	/**
	 * Turns car left or right 
	 */
	public int turn(int angle, SteeringWheel sw);
}