package car;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import car.components.ControlPanel;
import car.components.Vehicle;

/**
 * Class for testing the car
 */
public class MainTest {

        public static void main(String[] args) throws InterruptedException {
                ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
        Vehicle vehicle = context.getBean(Vehicle.class);
        
        ControlPanel cp = vehicle.getControl();
        
        cp.pourFuel(10);
        cp.start();
        cp.speedUp(9000);
        Thread.sleep(9100);
        
        cp.useHorn();
        Thread.sleep(3100);
        
        cp.turning(30);
        Thread.sleep(2100);
        
        cp.speedUp(4000);
        Thread.sleep(4100);
        
        cp.turning(-30);
        Thread.sleep(2100);
        
        cp.showSpeed();
        cp.stop();
        }
}