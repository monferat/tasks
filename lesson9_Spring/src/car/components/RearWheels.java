package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class RearWheels implements StatusAware{

	private int power = 0;
	
	@Override
	public void printStatus() {
		System.out.println("Power of brake = " + power + ".");
	}
}