package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class BrakePedal implements StatusAware{

	final private int power = 10;
	private boolean status = false;
	
	/** Gets power of brake	 */
	public int getPower() {
		return power;
	}
	
	/** Press brake pedal */
	public void pressPedal () {
		status = true;
	}
	
	/** Release brake pedal */
	public void stopPedal () {
		status = false;
	}
	
	public boolean isStatus() {
		return status;
	}

	@Override
	public void printStatus() {
		if (status){
			System.out.println ("Brake pedal is pressed.");
		} else {
			System.out.println ("Brake pedal is unpressed.");
		}
	}
}