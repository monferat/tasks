package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;
import car.interfaces.Steared;

@Component
public class ForwardWheels implements StatusAware, Steared {

	private int angleOfWheels = 0;
	private int power = 0;

	@Override
	public int turn(int angleSteeringWheel, SteeringWheel sw) {
		if (angleSteeringWheel > 0) {
			sw.turnRight(angleSteeringWheel);
		} else {
			sw.turnLeft(Math.abs(angleSteeringWheel));
		}
		this.angleOfWheels = sw.getAngle() / 2;
		return this.angleOfWheels;
	}

	@Override
	public void printStatus() {
		System.out.println("Angle of wheels = " + angleOfWheels
				+ "; Power of brake = " + power + ".");
	}

}
