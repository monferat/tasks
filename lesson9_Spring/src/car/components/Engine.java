package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class Engine implements StatusAware{

	private boolean status = false;
	final private int maxSpeed = 310;
	
	/**
	 * Gets max speed of car (km/h)
	 */
	public int getMaxSpeed() {
		return maxSpeed;
	}

	/**
	 * Set engine in the car
	 */
	public void setEngine (String s) {
		
		if ("on".equalsIgnoreCase(s))
		    status = true;
		if("off".equalsIgnoreCase(s))
			status = false;
	}
	
	/**
	 * Gets status of engine
	 */
	public boolean isStatus() {
		return status;
	}

	public void printStatus(String s) {
		if (status){
			System.out.println ("Engine is on.");
		} else {
			System.out.println ("Engine is off." + s);
		}
	}
	
	@Override
	public void printStatus() {
		if (status){
			System.out.println ("Engine is on.");
		} else {
			System.out.println ("Engine is off.");
		}
	}
}