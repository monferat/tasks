package car.components;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class ControlPanel implements StatusAware {

	@Autowired
	private Accelerator accelerator;

	@Autowired
	private BrakePedal brakePedal;

	@Autowired
	private Engine engine;

	@Autowired
	private ForwardWheels fw;

	@Autowired
	private GasTank gasTank;

	@Autowired
	private HandBrake handBrake;

	@Autowired
	private Horn horn;

	@Autowired
	private RearWheels rw;

	@Autowired
	private SteeringWheel sw;

	private double currentSpeed;
	private double currentVolume = 0;
	private int currentAngle;
	private double distance = 0;

	/**
	 * Initializes all elements of the car
	 */
	@PostConstruct
	public void init() {
		accelerator.printStatus();
		brakePedal.printStatus();
		engine.printStatus();
		fw.printStatus();
		gasTank.printStatus();
		handBrake.printStatus();
		horn.printStatus();
		rw.printStatus();
		sw.printStatus();
	}
	
	@Override
	public void printStatus() {
		System.out.println("Current speed = " + currentSpeed
				+ " km/h; current volume = " + currentVolume
				+ " l; current angle = " + currentAngle + "; distance = "
				+ distance + " m\n");
	}

	/**
	 * Engages engine of the car
	 */
	public void start() {
		final double usingGas = 5;
		if (!engine.isStatus()) {
			engine.setEngine("on");
			currentSpeed = 0;
			currentAngle = 0;
			engine.printStatus();
			System.out.println();
			this.printStatus();
			Thread thread = new Thread() {
				public void run() {
					while (engine.isStatus()) {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						if (currentSpeed > 0) {
							useFuel(currentSpeed * (usingGas / 100000));
							distance += currentSpeed;
						} else {
							useFuel(0.01);
						}
					}
				}
			};
			thread.start();
		} else {
			System.out.println("Engine is active, you can not to start it!");
		}
	}

	/**
	 * Stops the car and turn off the engine
	 */
	public void stop() {
		if (engine.isStatus()) {
			engine.setEngine("off");
			currentSpeed = 0;
			engine.printStatus();
			this.printStatus();
		} else {
			System.out.println("Engine is not active, you can not to stop it!");
		}
	}

	/**
	 * Stops engine and show message when you have no gas
	 */
	private void noFuel() {
		engine.setEngine("off");
		engine.printStatus("You have not fuel");
	}

	/**
	 * Use horn of the car during 3 seconds
	 */
	public void useHorn() {
		Thread thread = new Thread() {
			public void run() {
				horn.HornButton();
				horn.printStatus();
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				if (engine.isStatus()) {
					horn.stopHornButton();
					horn.printStatus();
				}
			}
		};
		thread.start();
	}

	/**
	 * Use gas from gas tank for work of engine
	 */
	private void useFuel(final double volume) {
		if (engine.isStatus()) {
			gasTank.useGas(volume);
			currentVolume = gasTank.getCurrentVolume();
			if (currentVolume <= 0) {
				currentVolume = 0;
				noFuel();
			}
			gasTank.printStatus();
		}
	}

	/**
	 * Speed up the car pushing accelerate pedal 
	 */
	public void speedUp(final long timeSpeedUp) {
		Thread thread = new Thread() {
			public void run() {
				long currentTime = 0;
				if (currentSpeed < engine.getMaxSpeed() && engine.isStatus()) {
					accelerator.pressPedal();
					accelerator.printStatus();
					System.out.println();
					while (engine.isStatus()
							&& currentSpeed <= engine.getMaxSpeed()
							&& currentTime < timeSpeedUp) {
						useFuel(0.01);
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						currentTime += 1000;
						currentSpeed += 20;
						if (currentSpeed > engine.getMaxSpeed()) {
							currentSpeed = engine.getMaxSpeed();
						}
						showSpeed();
					}
					if (engine.isStatus()) {
						accelerator.stopPedal();
						accelerator.printStatus();
						printStatus();
					}
				} else {
					System.out.println("Speed is maximum now.");
					printStatus();
				}
			}
		};
		thread.start();
	}

	/** Prints current speed of car 	 */
	public void showSpeed() {
		System.out.println("Current speed = " + currentSpeed + " km/h\n");
	}

	/** Add gas to gasTank */
	public void pourFuel(int vol){
		gasTank.fillGas(vol);
	}
	
	/**
	 * Turns car left or right owing to using steering wheel
	 */
	public void turning(final int angle) {
		Thread thread = new Thread() {
			public void run() {
				if (currentSpeed > 0 && engine.isStatus()) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					if (engine.isStatus()) {
						fw.turn(angle, sw);
						currentAngle = sw.getAngle();
						sw.printStatus();
						currentSpeed -= 5;
						if (currentSpeed < 0) {
							currentSpeed = 0;
						}
						printStatus();
					}
				} else {
					System.out.println("Car is stoped.");
					printStatus();
				}
			}
		};
		thread.start();
	}
}