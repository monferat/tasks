package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class SteeringWheel implements StatusAware{
	
	final private int maxAngle = 130;
	private int angle = 0;
	
	/**
	 * Turns steering wheel left 
	 */
	public void turnLeft(int angle){
		this.angle -= angle;
		if (Math.abs(this.angle) > maxAngle){
			this.angle = -maxAngle;
		}
	}
	
	/**
	 * Turns steering wheel right 
	 */
	public void turnRight(int angle){
		this.angle += angle;
		if (this.angle > maxAngle){
			this.angle = maxAngle;
		}
	}

	/**
	 * Gets current angle 
	 */
	public int getAngle() {
		return angle;
	}

	@Override
	public void printStatus() {
		if (angle < 0){
			System.out.println("Car turning left.");
		} else if (angle > 0){
			System.out.println("Car turning right.");
		}
		System.out.println ("Angle of steering wheel = " + angle + ".");
	}
}