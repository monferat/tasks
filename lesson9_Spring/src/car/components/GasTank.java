package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class GasTank implements StatusAware{
	
	final private double maxVolume = 50;
	private double currVolume = 0;
	
	/**
	 * Fill gas tank
	 */
	public void fillGas(double volume){
		if(volume < maxVolume){
			this.currVolume += volume;
		}
		if(this.currVolume > maxVolume){
			this.currVolume = maxVolume;
		}
	}
	
	/**
	 * Uses some volume of gas in gas tank
	 */
	public double useGas(double volume){
		if(volume > 0){
			this.currVolume -= volume;
		}
		if(this.currVolume <= 0){
			this.currVolume = 0;
		}
		return currVolume;
	}
	
	public double getCurrentVolume () {
		return currVolume;
	}
	
	@Override
	public void printStatus() {
		System.out.printf ("Fuel volume is %.3f\n",currVolume );
	}
}
