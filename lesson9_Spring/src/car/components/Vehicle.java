package car.components;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class Vehicle implements StatusAware{

	@Autowired 
	private ControlPanel control;
	
	/**
	 * Gets Control Panel for control of the car
	 */
	public ControlPanel getControl() {
		return control;
	}

	@Override
	public void printStatus() {
		System.out.println ();
	}
}