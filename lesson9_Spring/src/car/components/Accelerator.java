package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class Accelerator implements StatusAware{

	private boolean status = false;
	
	public void pressPedal () {
		status = true;
	}
	
	public void stopPedal () {
		status = false;
	}
	
	/** Gets status of accelerate pedal */
	public boolean isStatus() {
		return status;
	}

	@Override
	public void printStatus() {
		if (status){
			System.out.println ("Accelerator pedal is pressed.");
		} else {
			System.out.println ("Accelerator pedal is unpressed.");
		}
	}
}