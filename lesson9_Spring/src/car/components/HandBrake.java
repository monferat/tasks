package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class HandBrake implements StatusAware{

	final private int power = 20;
	private boolean status = false;
	
	/**
	 * Gets power of brake
	 */
	public int getPower() {
		return power;
	}

	/** Press hand brake */
	public void pressHandBrake () {
		status = true;
	}
	
	/** Release hand brake */
	public void stopPressHandBrake () {
		status = false;
	}
	
	/** Gets status of hand brake */
	public boolean isStatus() {
		return status;
	}

	@Override
	public void printStatus() {
		if (status){
			System.out.println ("Hand brake is using now.");
		} else {
			System.out.println ("Hand brake is unusing now.");
		}
	}
}