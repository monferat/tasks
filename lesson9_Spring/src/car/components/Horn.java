package car.components;

import org.springframework.stereotype.Component;

import car.interfaces.StatusAware;

@Component
public class Horn implements StatusAware{
	
	private boolean status = false;
	
	/** Press horn */
	public void HornButton () {
		status = true;
	}
	
	/** Release horn button	 */
	public void stopHornButton () {
		status = false;
	}
	
	/** Gets status of horn	 */
	public boolean isStatus() {
		return status;
	}

	@Override
	public void printStatus() {
		if (status){
			System.out.println ("Horn is using now.\n");
		} else {
			System.out.println ("Horn is unusing now.\n");
		}
	}
}