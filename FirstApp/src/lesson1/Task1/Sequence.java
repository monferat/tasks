package lesson1.Task1;

/*  ����� Sequence*/

public class Sequence {

	/* ����� ������ ������������������ ��������� */
	public static int fibonacci(int n) {

		int fn_1 = 0;
		int fn_2 = 1;
		int fn;

		if (n == 0) {
			System.out.println("������������������ ���������: ");
			System.out.println(0);
			return 1;
		}

		if (n < 0) {
			System.out.println("������! n - ������ ���� ������ ����");
			return 1;
		}

		System.out.println("������������������ ���������: ");
		System.out.println(fn_2 + " ");

		/* �������� �������� */
		for (int i = 1; i < n; i++) {

			fn = fn_1 + fn_2;
			fn_1 = fn_2;
			fn_2 = fn;

			System.out.println(fn + " ");
		}

		return 0;
	}

	/* ����� ������ ������������������ ������ */
	public static int fraction(int n) {

		if (n <= 0) {
			System.out.println("������! n - ������ ���� ������ ����");
			return 1;
		}

		System.out.println("������������������ ������: ");

		for (int i = 1; i <= n; i++) {
			System.out.printf("%.4f\n", 1.0 / i);
		}

		return 0;
	}

	/* ����� ������ ������������������ �������� ASCII */
	public static int charSeq(int m, int n) {

		if (n < 0 || m < 0) {
			System.out.println("������! m � n - ������ ���� ��������������");
			return 1;
		}

		System.out.println("������������������ �������� ASCII: ");

		if (m > n) {
			for (int i = m; i >= n; i--) {
				System.out.printf("%d = %c\n", i, i);
			}
		} else {
			for (int i = m; i <= n; i++) {
				System.out.printf("%d = %c\n", i, i);
			}
		}

		return 0;
	}

}