package lesson1.Task1;

/**
 * GeekHub. Lesson1 
 * Task 1.1 (FibonacciSequence)
 *  
 * @author ���������� ��������
 *
 * 20 Oct 2012. 
 */

import java.util.*;

public class FibonacciSequence {

	private static Scanner in;

	/**
	 * @param args
	 * 
	 *            ����� n - ����� ������������������ ���������
	 */
	public static void main(String[] args) {

		in = new Scanner(System.in);
		System.out.println("������� ����� ����� n:");

		try {
			int n = in.nextInt();
			Sequence.fibonacci(n);
		} catch (InputMismatchException e) {
			throw new InputMismatchException(
					"������! n - ������ ���� ����� ����������� ������");
		}
	}

}
