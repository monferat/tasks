package lesson1.Task2;

/**
 * GeekHub. Lesson1 
 * Task 1.4 (Sum)
 *  
 * @author ���������� ��������
 *
 * 21 Oct 2012. 
 */

import java.util.*;

public class Sum {

	private static Scanner in;

	/**
	 * @param args
	 * 
	 *            ������������ ���� �����
	 */
	public static void main(String[] args) {

		float sum;
		in = new Scanner(System.in);

		System.out.println("������� ����� m � n:");

		String m = in.nextLine();
		String n = in.nextLine();

		sum = Operations.sum(check(m), check(n));

		System.out.println("Sum = " + sum);

	}

	// ���������, � ����� ������� ������ �����(0x - 16-������, 0 - 8-������))
	static float check(String n) {

		if ((n.charAt(0) == '0')
				&& ((n.charAt(1) == 'x') || (n.charAt(1) == 'X')))
			return toDecimal(n.substring(2, n.length()), 16);
		else if (n.charAt(0) == '0')
			return toDecimal(n.substring(1, n.length()), 8);
		else
			return Float.parseFloat(n);
	}

	// ��������� ����� � ���������� �������
	static float toDecimal(String number, int radix) {

		int index1 = number.indexOf('8');
		int index2 = number.indexOf('9');

		if ((radix == 8) && (index1 != -1 || index2 != -1)) {
			System.out.println("����������� ���� 8-�� �������:");
			return -1;
		} else
			return (float) Integer.parseInt(number, radix);

	}

}
