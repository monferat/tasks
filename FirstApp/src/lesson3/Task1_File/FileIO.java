/**
 * 
 */
package lesson3.Task1_File;

import java.io.File;
import java.io.IOException;

/**
 * GeekHub. Lesson2 
 * Task 3.1 (Working with file)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 2 Nov 2012. 
 */
public class FileIO {

	/**
	 * @param args
	 * 
	 * Creating, renaming and deleting directory
	 */
	public static void main(String[] args){
		
		if(args.length <=1){
			System.out.println("Error! Use [1 ,2, 3] 'directory name' 'if [2] dir rename'");
			System.out.println("1 - create dir\n2 - rename dir\n3 - delete dir");   
		}
		else
			try {
				doFile(args);
			} catch (IOException e) {			
				e.printStackTrace();
			}
		
	}
	
	/* method  doFile have 3 cases
	 * 1 - make directory if it not exist 
	 * 2 - rename directory if it exist
	 * 3 - delete directory if it exist   
	 *  */
	public static void doFile(String[] args) throws IOException {
		
	   File tempDir = new File(File.separator + args[1]);
		
		switch(args[0]){
		
		// Creating of the directory 
			case "1":{
				if(tempDir.exists() == false){
					if(tempDir.mkdir())
						System.out.println("Direcory " +args[1]+" is created");
					else
						System.out.println("Error! Direcory is not created");
				}else
					System.out.println("Error! Direcory is existing");
				break;
			}
			
		// Renaming the directory	
			case "2":{
				File newTempDir = new File(File.separator + args[2]);
				if(tempDir.exists() == true){
					if(tempDir.renameTo(newTempDir))
	  	  				System.out.println("Direcory is renamed");
	  	  			else
	  	  				System.out.println("Error! Direcory is not renamed");
				}else
					System.out.println("Error! Direcory with the same name is existing");
				break;
			}
			
		// Deliting the directory		
			case "3":{
				if(tempDir.exists() == true){
					if(tempDir.delete())
						System.out.println("Direcory "+args[1]+" is deleted");
					else
						System.out.println("Error! Direcory is not deleted");
				}else
					System.out.println("Error! Direcory is not exist");
				break;
			}
		}

	}

}
