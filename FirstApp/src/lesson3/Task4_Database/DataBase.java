/**
 * 
 */
package lesson3.Task4_Database;

import java.sql.*;
import java.util.Scanner;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Statement;

/**
 * GeekHub. Lesson2 
 * Task 3.4 (Working with database)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 3 Nov 2012. 
 * 
 * Receive query from database
 * View format results of query
 * Update with DELETE, INSERT, UPDATE
 */
public class DataBase {

	private static Scanner scan;     // use for scan query line

	/**
	 * @param args
	 * @throws SQLException
	 */
	public static void main(String[] args) throws SQLException {
		
		String r = "";
		scan = new Scanner(System.in);
        
		//Receive the initial database parameters 
		System.out.print("Input host(for example 127.0.0.1:3306): ");
		String url = "jdbc:mysql://" + scan.nextLine() + "/";
		System.out.print("Input database name: ");
		String dataBaseName = scan.nextLine();
		System.out.print("Input username: ");
		String username = scan.nextLine();
		System.out.print("Input password: ");
		String password = scan.nextLine();

		Connection conn = null;

		//connect to database
		conn = (Connection) DriverManager.getConnection(url + dataBaseName,
				username, password);

		if (conn == null) {
			System.out.println("No connection to DataBase!");
			System.exit(0);
		} else
			System.out.println(" DataBase connection - OK !\n");

		Statement stmt = (Statement) conn.createStatement();
		
		System.out.println("Enter request(print 'exit' for exit): ");
		r = scan.nextLine();
        
		//scan query one by one while not 'exit'
		while (!r.equals("exit")) {

			Request req = new Request(r);

			// SELECT query
			if (r.startsWith("select") || r.startsWith("SELECT")) {
				req.select(stmt);
			}

			// other query(UPDATE,INSERT,DELETE,..)
			else {
				req.update(stmt);
			}

			System.out.println("\nEnter request(print 'exit' for exit): ");
			r = scan.nextLine();
		}

		/**
		 * stmt.close(); When close Statement automatically close all connected
		 * ResultSet objects that was open
		 */
		stmt.close();
		conn.close();
	}

}
