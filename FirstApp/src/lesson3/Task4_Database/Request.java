/**
 * 
 */
package lesson3.Task4_Database;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.Statement;

/**
 * GeekHub. Lesson2 
 * Task 3.4 (Working with database)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 3 Nov 2012. 
 * 
 * Receive query from database
 * View format results of query
 * Update with DELETE, INSERT, UPDATE
 * 
 * Request class
 */

public class Request {
	private String request;

	// Constructor
	public Request(String request) {
		this.request = request;
	}

	// get method
	public String getRequest() {
		return request;
	}

	// set method
	public void setRequest(String request) {
		this.request = request;
	}

	// SELECT method
	public void select(Statement statement) {
		
		String query = request;
		
		try {
			ResultSet result = statement.executeQuery(query);
			viewTable(result);
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	// UPDATE, INSERT, DELETE, etc method
	public void update(Statement statement) {
		try {
			int rowsAffected = statement.executeUpdate(request);
			System.out.println("Total updates:" + rowsAffected
					+ "\naction done succesfull");
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	// print a table from database
	private void viewTable(ResultSet rs) throws SQLException {

		int count = rs.getMetaData().getColumnCount(); // get count of columns in table

		// print the column names
		for (int i = 1; i < count; i++)
			System.out.printf("%-25s", rs.getMetaData().getColumnName(i));
		System.out.println();
		for (int i = 1; i < count; i++)
		System.out.print("________________________");
		System.out.println();

		// print the row data
		while (rs.next()) {
			System.out.printf("|%-25s", rs.getRow());

			for (int i = 2; i < count; i++) {
				System.out.printf("|%-25s", rs.getString(i));
			}
			System.out.println();
		}

	}
}
