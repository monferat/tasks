/**
 * 
 */
package lesson3.Task2_Encode;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;

/**
 * GeekHub. Lesson2 Task 3.2 (Switch coding of file)
 * 
 * @author Vladimir Bezpalchuck
 * 
 *         2 Nov 2012.
 */
public class FileCoding {

	// ArrayList using for containing of encodes list
	private static ArrayList<String> codeSystem = new ArrayList<>();

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		setCode();

		if (args.length < 3) {
			System.out.println("Error! Use: 'file name' 'original code system' 'new code system'");
			System.out.println("List of code system:");
			print();
		} else {
			recodeFile(args); // encoding file
		}
	}

	/**
	 *  Change encoding system of input file
	 */
	public static void recodeFile(String[] args) {

		try {

			Reader reader = new InputStreamReader(new FileInputStream(args[0]),args[1]);
			Writer writer = new OutputStreamWriter(new FileOutputStream("new" + args[0]), args[2]);

			int c = 0;
			while ((c = reader.read()) >= 0)
				writer.write(c);

			reader.close(); // close file
			writer.close(); // close file
			System.out.println("All done");

		} catch (IOException e) {
			System.out.println("Error open file");
			e.fillInStackTrace();
		}

	}

	// List of encodes
	public static void setCode() {
		codeSystem.add("US-ASCII");
		codeSystem.add("ISO-8859-1");
		codeSystem.add("UTF-8");
		codeSystem.add("UTF-16");
		codeSystem.add("ISO-8859-2");
		codeSystem.add("ISO-8859-4");
		codeSystem.add("ISO-8859-5");
		codeSystem.add("ISO-8859-7");
		codeSystem.add("ISO-8859-9");
		codeSystem.add("windows-1250");
		codeSystem.add("windows-1251");
		codeSystem.add("windows-1252");
		codeSystem.add("windows-1253");
		codeSystem.add("windows-1254");
		codeSystem.add("windows-1257");
		codeSystem.add("Cp850");
		codeSystem.add("Cp852");
		codeSystem.add("Cp855");
		codeSystem.add("Cp856");
		codeSystem.add("Cp866");
		codeSystem.add("Cp875");
	}

	// Print the list of encodes
	public static void print() {
		for (String s : codeSystem)
			System.out.println(s);
	}

}
