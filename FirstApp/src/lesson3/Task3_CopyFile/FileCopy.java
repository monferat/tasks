/**
 * 
 */
package lesson3.Task3_CopyFile;

import java.io.*;

/**
 * GeekHub. Lesson2 Task 3.3 (Copy a file)
 * 
 * @author Vladimir Bezpalchuck
 * 
 *         2 Nov 2012.
 * 
 *         Copy of file using Stream and Buffered Stream. Measurement time of
 *         the program work and Comparison of methods copying files
 */
public class FileCopy {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		if (args.length < 3) {
			System.out.println("Error! Use: '[1 2]' 'file1' 'file2'");
			System.out.println("1 - Buffered copy\n2 - Unbuffered copy");
		} else {
			switch (args[0]) {

			case "1": {
				long start = System.nanoTime(); // start timer
				copyFile(args); // copy a file
				long finish = System.nanoTime(); // stop timer
				System.out.println("Elapsed time of copy: " + (finish - start) / 10e6 + " ms");
				break;
			}

			case "2": {
				long start = System.nanoTime(); // start timer
				copyFileBuf(args); // copy a file using a buffer
				long finish = System.nanoTime(); // stop timer
				System.out.println("Elapsed time of copy buffer vesrion: " + (finish - start) / 10e6 + " ms");
				break;
			}
			}
		}

	}

	// Use buffered streams to copy a file
	public static void copyFile(String[] args) {

		try {
			File f1 = new File(args[1]);
			File f2 = new File(args[2]);

			InputStream fin = new FileInputStream(f1);
			OutputStream fout = new FileOutputStream(f2);

			// reading from file1 and writing to file2
			byte[] b = new byte[128];
			int len;
			while ((len = fin.read(b)) > 0) {
				fout.write(b, 0, len);
			}

			fin.close(); // close file
			fout.close(); // close file

			System.out.println("All done. File copied");

		} catch (FileNotFoundException ex) {
			System.out
					.println(ex.getMessage() + " in the specified directory.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error open file");
			e.fillInStackTrace();
		}

	}

	// Use simple streams to copy a file
	public static void copyFileBuf(String[] args) {

		try {
			BufferedInputStream fin = new BufferedInputStream(
					new FileInputStream(args[1]));
			BufferedOutputStream fout = new BufferedOutputStream(
					new FileOutputStream(args[2]));

			// reading from file1 and writing to file2
			int c;
			do {
				c = fin.read();
				if (c != -1)
					fout.write(c);
			} while (c != -1);

			fin.close(); // close file
			fout.close(); // close file

			System.out.println("All done. File copied");

		} catch (FileNotFoundException ex) {
			System.out
					.println(ex.getMessage() + " in the specified directory.");
			System.exit(0);
		} catch (IOException e) {
			System.out.println("Error open file");
			e.fillInStackTrace();
		}

	}

}
