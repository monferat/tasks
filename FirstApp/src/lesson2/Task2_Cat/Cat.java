/**
 * 
 */
package lesson2.Task2_Cat;

/**
 * GeekHub. Lesson2 Task 2.2 (Class Cat)
 * 
 * @author Vladimir Bezpalchuck
 * 
 *         28 Oct 2012.
 */

public class Cat {

	/**
	 * @param args
	 */
	int[] rgbColor = new int[3];

	int age;

	public static void main(String[] args) {

		Cat cat1 = new Cat(2, 4, 5, 15);

		Cat cat2 = new Cat(2, 4, 5, 15);
		Cat cat3 = new Cat(2, 6, 5, 15);

		System.out.println(cat1.toString());
		System.out.println("cat1 == cat2: " + cat1.equals(cat2));
		System.out.println("cat2 == cat3: " + cat2.equals(cat3));

		System.out.println("cat2 == cat1: " + cat2.equals(cat1));
		System.out.println("cat1 == cat1: " + cat1.equals(cat1));
		System.out.println("cat1 == null: " + cat1.equals(null));
		System.out.println("cat1 == abc: " + cat1.equals("abc"));

		System.out.println("cat1 hash: " + cat1.hashCode() + "\ncat2 hash: "
				+ cat2.hashCode());

	}

	// Constructor 1
	public Cat(int[] RGB, int age) {

		this.age = age;

		for (int i = 0; i < 3; i++)
			rgbColor[i] = RGB[i];
	}

	// Constructor 2
	public Cat(int R, int G, int B, int age) {

		this.age = age;

		rgbColor[0] = R;
		rgbColor[1] = G;
		rgbColor[2] = B;
	}

	// Overriding the equals method
	public boolean equals(Object kitty) {

		// Quick check for identity
		if (this == kitty)
			return true;

		// Check for null
		if (kitty == null)
			return false;

		// First checking the object for supporting with Cat
		if (kitty instanceof Cat) {

			// Type conversion
			Cat k = (Cat) kitty;

			return (k.age == age && k.rgbColor[0] == rgbColor[0]
					&& k.rgbColor[1] == rgbColor[1] && k.rgbColor[2] == rgbColor[2]);
		}

		return false;

	}

	// Overriding the hashCode method
	public int hashCode() {
		return new Integer(age).hashCode()
				+ new Integer(rgbColor[0] + rgbColor[1] + rgbColor[2]).hashCode();
	}

	// Overriding the toString method
	public String toString() {

		return "colorRGB(" + rgbColor[0] + "," + rgbColor[1] + ","
				+ rgbColor[2] + ") age:" + age + " " + super.toString();

	}

}
