/**
 * GeekHub. Lesson2 
 * Task 2.4 (Class MyStack use the Queue interface)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 29 Oct 2012. 
 */

package lesson2.Task4_Stack;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

;

public class MyStack<Item> implements Queue<Item> {

	@SuppressWarnings("unchecked")
	private Item[] a = (Item[]) new Object[1]; // stack of items
	private int N = 0;

	public boolean isEmpty() {
		return N == 0;
	}

	public int size() {
		return N;
	}

	@Override
	// Add item to top of stack
	public boolean add(Item item) {

		if (N == a.length)
			resize(2 * a.length); // Check a size of stack
		a[N++] = item;
		return true;
	}

	@Override
	// Retrieves and removes item from top of stack if it is possible
	public Item poll() {

		if (this.isEmpty()) {
			return null;
		}

		Item item = a[--N];
		a[N] = null;
		if (N > 0 && N == a.length / 4)
			resize(a.length / 2); // Check a size of stack

		return item;
	}

	@Override
	// Retrieves, but does not remove top of stack
	public Item peek() {
		if (this.isEmpty()) {
			return null;
		}
		return a[N - 1];
	}

	// Move stack to a new array of size max
	private void resize(int max) {

		@SuppressWarnings("unchecked")
		Item[] temp = (Item[]) new Object[max];
		System.arraycopy(a, 0, temp, 0, N);
		a = temp;
	}

	@Override
	// Retrieves, but does not remove top of stack
	public Item element() {
		return a[N - 1];
	}

	@Override
	// Remove & retrieve item from top of stack
	public Item remove() {

		Item item = a[--N];
		a[N] = null;
		if (N > 0 && N == a.length / 4)
			resize(a.length / 2); // Check a size of stack

		return item;
	}

	// Print stack
	public void print() {
		System.out.print("[ ");
		for (int i = 0; i < N; i++)
			System.out.print(a[i] + " ");
		System.out.println("]");
	}

	@Override
	public boolean offer(Item item) {
		return false;
	}

	@Override
	public boolean contains(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object[] toArray() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <T> T[] toArray(T[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection<? extends Item> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection<?> c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub

	}

	/**
	 * Main method
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		MyStack<String> stack = new MyStack<>();
		System.out.println(stack.poll());
		System.out.println(stack.peek());
		System.out.print("Empty: ");
		stack.print();

		stack.add("Acer");
		stack.add("Dell");
		stack.add("Samsung");
		stack.add("Asus");

		System.out.print("After add: -> ");
		stack.print();

		System.out.print("peek: " + stack.peek() + " -> ");
		stack.print();

		System.out.print("poll: " + stack.poll() + " -> ");
		stack.print();

		System.out.print("remove: " + stack.remove() + " -> ");
		stack.print();

	}

}
