/**
 * Test exception that extends RuntimeException
 * call if flag == 0
 */
package lesson2.Task3_Exceptions;

@SuppressWarnings("serial")
public class MyRuntimeException extends RuntimeException{

	public MyRuntimeException() {
		super("MyRuntimeException: Incorect expression, flag is 0");
	}

	public MyRuntimeException(String arg) {
		super(arg);
	}
}
