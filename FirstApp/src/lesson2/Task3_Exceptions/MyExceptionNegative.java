/**
 * Test exception that extends Exception
 * call if number in method < 0
 */
package lesson2.Task3_Exceptions;

@SuppressWarnings("serial")
public class MyExceptionNegative extends Exception{
	

	public MyExceptionNegative(String s) {
		
		super(s);
		this.message(s);
		
	}
	
	public void message(String s){
		
		System.out.println(s);
	}
	
}
