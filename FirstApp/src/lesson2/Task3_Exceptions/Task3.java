/**
 * 
 */
package lesson2.Task3_Exceptions;

/**
 * GeekHub. Lesson2 Task 2.3 (Test using of Exception and RuntimeException)
 * 
 * @author Vladimir Bezpalchuck
 * 
 *         28 Oct 2012.
 */

public class Task3 {
	public static void main(String[] args) {

		double a = Double.parseDouble(args[0]);
		int flag = 0;

		// test call of first exception
		try {
			System.out.println("sqrt(" + a + ")=" + mySqrt(a));
		} catch (MyExceptionNegative e1) {
			e1.printStackTrace();
		}

		// test call of second exception(Runtime exception)
		try {
			testM(flag);
		} catch (MyRuntimeException e2) {
			System.out.println(e2);
		}

	}

	// the method that have an exception
	public static double mySqrt(double a) throws MyExceptionNegative {
		if (a >= 0) {
			return Math.sqrt(a);
		} else {
			throw new MyExceptionNegative("Number " + a + " is negative");
		}
	}

	// the method that have an exception 2
	public static void testM(int flag) {
		if (flag == 0)
			throw new MyRuntimeException();
	}

}
