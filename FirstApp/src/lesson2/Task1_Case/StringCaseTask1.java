/**
 * 
 */
package lesson2.Task1_Case;

/**
 * GeekHub. Lesson2 Task 2.1 (String case)
 * 
 * @author Vladimir Bezpalchuck
 * 
 *         28 Oct 2012.
 */
public class StringCaseTask1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String str = args[0];
		System.out.println(str);

		System.out.println(checkStr(str));

	}

	// the method modifying a case of the first character
	public static String checkStr(String s) {

		char c = s.charAt(0); // take the first character of an input string
		String s2 = c + "";

		// Checking string case
		if (Character.isUpperCase(c)) {
			s2 = s2.toLowerCase();
			return s = s2 + s.substring(1);
		} else {
			s2 = s2.toUpperCase();
			return s = s2 + s.substring(1);
		}
	}

}
