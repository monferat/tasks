/**
 * 
 */
package lesson2.Task5_VoteBox;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * GeekHub. Lesson2 Task 2.5 (Vote box)
 * 
 * @author Vladimir Bezpalchuck
 * 
 *         29 Oct 2012.
 */

@SuppressWarnings("rawtypes")
public class VoteBox implements Collection {

	private final String s1 = "Vasia Pupkin";
	private final String s2 = "Jone Dow";

	private ArrayList<String> a = new ArrayList<String>(); // using for VoteBox

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		VoteBox vBox = new VoteBox();
		vBox.print();

		vBox.add("Vasia Pupkin");
		vBox.add("Anton Pupkin");
		vBox.add("Jone Dow");
		vBox.add("Vasia Pupkin");
		vBox.add("Vasia Ololoev");
		vBox.add("Jone Dow");

		vBox.print();

	}

	@Override
	public int size() {

		return a.size();
	}

	@Override
	public boolean isEmpty() {

		return a.isEmpty();
	}

	@Override
	public boolean contains(Object o) {

		return a.contains(o);
	}

	@Override
	public Iterator<String> iterator() {

		return a.iterator();
	}

	@Override
	public Object[] toArray() {

		return a.toArray();
	}

	@Override
	public Object[] toArray(Object[] a) {
		// TODO Auto-generated method stub
		return null;
	}

	// Special add for box
	@Override
	public boolean add(Object e) {

		String str = (String) e;

		if (str.compareTo(s1) == 0) {
			return false;
		} else if (str.compareTo(s2) == 0) {
			a.add(str);
			a.add(str);
			return true;
		} else {
			a.add(str);
			return true;
		}

	}

	@Override
	public boolean remove(Object o) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean containsAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeAll(Collection c) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean retainAll(Collection c) {

		return a.retainAll(c);
	}

	@Override
	public void clear() {

		a.clear();
	}

	public void print() {
		System.out.println(a);
	}

}
