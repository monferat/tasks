/**
 * 
 */
package lesson2.Task6_Cars;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * GeekHub. Lesson2 
 * Task 2.6 (Sorting of cars)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 29 Oct 2012. 
 */

public class Car implements Comparable<Car> {
	
	public String brand;
	public Integer baseLong;
	public String color;
	
	public Car(String brand, Integer baseLong, String color){
		this.brand = brand;
		this.baseLong = baseLong;
		this.color = color;
	}

	@Override
	public int compareTo(Car acar) {
		return brand.compareTo(acar.brand);
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
	    List<Car> cars = new ArrayList<>();
	    
	    cars.add(new Car("Cadillac", 13, "white"));
	    cars.add(new Car("Honda", 12,"black"));
	    cars.add(new Car("Renault", 15,"blue"));
	    cars.add(new Car("Audi", 5,"red"));
	    
		
		Collections.sort(cars, new Comparator<Car>() {
	        @Override
	        public int compare(Car car1, Car car2) {
	            return car1.color.compareTo(car2.color);
	        }
		});
		
		System.out.println("First Sort by color:");
		for (Car car : cars) {
	        System.out.println(car.brand+" "+car.baseLong+" "+car.color);
	    }  
		
		Collections.sort(cars, new Comparator<Car>() {
	        @Override
	        public int compare(Car car1, Car car2) {
	            return car1.brand.compareTo(car2.brand);
	        }
		});
		
		System.out.println("\nSecond Sort by brand:");
		for (Car car : cars) {
	        System.out.println(car.brand+" "+car.baseLong+" "+car.color);
	    }  
		
		Collections.sort(cars, new Comparator<Car>() {
	        @Override
	        public int compare(Car car1, Car car2) {
	            return car1.baseLong.compareTo(car2.baseLong);
	        }
		});
		
		System.out.println("\nThird Sort by long of base:");
		for (Car car : cars) {
	        System.out.println(car.brand+" "+car.baseLong+" "+car.color);
	    }  

    }
}
