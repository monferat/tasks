/**
 * 
 */
package lesson4.Task2_URL;

/**
 * GeekHub. Lesson4 
 * Task 4.2 (Print listing of links by it 
 * response code priority)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 11 Nov 2012. 
 * 
 */

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Scanner;

import org.htmlparser.Parser;
import org.htmlparser.filters.NodeClassFilter;
import org.htmlparser.tags.LinkTag;
import org.htmlparser.util.NodeList;
import org.htmlparser.util.ParserException;

/**
 * This program use an URL connection to communicate with a web server and check
 * all links on web page for its response code
 */
public class UrlGetData {

	private static ArrayList<String> listLink = new ArrayList<>();
	private static String protocol;
	private static String host;

	public static void main(String[] args) throws IOException {
		URLConnection connection = null;
		String urlString = readURLStr(args);
		try {
			URL u = new URL(urlString);
			connection = u.openConnection();
			protocol = u.getProtocol();
			host = u.getHost();

			// Check if response code is HTTP_OK (200)
			HttpURLConnection httpConnection = (HttpURLConnection) connection;
			if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK)
				return;
		} catch (IOException e) {
			System.out.println("Error" + e.getMessage());
			return;
		}

		Scanner in = new Scanner(connection.getInputStream());

		// find and save links
		while (in.hasNextLine()) {
			String input = in.nextLine();
			linkSearcher(input);
		}
		in.close();

		// get response code and print links
		printLink(listLink);

	}

	/**
	 * Method get input URL
	 */
	private static String readURLStr(String[] args) {
		String urlString;
		if (args.length == 1) {
			urlString = args[0];
		} else {
			urlString = "http://opera.com/"; // default value
		}
		System.out.println("Using: " + urlString);
		return urlString;
	}

	/**
	 * Method check the response code of links set Print list of links for given
	 * URL
	 */
	public static void printLink(ArrayList<String> list)
			throws MalformedURLException, IOException {

		ArrayList<String> badLink = new ArrayList<>();
		ArrayList<String> redirectLink = new ArrayList<>();
		ArrayList<String> goodLink = new ArrayList<>();

		for (String sLink : list) {

			int code = 0;
			try {
				// Open connection
				URL u = new URL(sLink);
				HttpURLConnection httpConnection = (HttpURLConnection) u
						.openConnection();
				httpConnection.setRequestMethod("HEAD");
				code = httpConnection.getResponseCode();
			} catch (IOException e) {
				System.out.println("Error" + e.getMessage());
			}

			// add to own list
			if (code >= 400)
				badLink.add(sLink);
			else if (code >= 300)
				redirectLink.add(sLink);
			else if (code >= 200)
				goodLink.add(sLink);
		}

		// print links by its code
		System.out.println("Good Links:");
		print(goodLink);
		System.out.println("Redirect Links:");
		print(redirectLink);
		System.out.println("Bad Links:");
		print(badLink);
	}

	// print list of data
	public static void print(ArrayList<String> list) {

		for (String s : list)
			System.out.println(s);

	}

	/*
	 * Method search links on web site and save them in the list
	 */
	public static void linkSearcher(String str) {

		int index = 0;

		str = str.trim();
		index = str.indexOf("<a"); // find <a
		if (index != -1) {
			index = str.indexOf("href=\""); // find href="
			if (index != -1) {
				str = str.substring(index + 6, str.length()); // start of link
				index = str.indexOf("\""); // catching link by finding of "
				if (index != -1) {
					str = str.substring(0, index);// catch link
					index = str.indexOf("http");
					if (index == -1) {
						str = protocol + "://" + host + str; // add protocol and
																// host if not
					}
					listLink.add(str); // add link to list
				}
			}
		}
	}

	/*
	 * Another additional method that search links on web site and save them in
	 * the list using HTML Parser
	 */
	public static ArrayList<String> linkSearcherHtmlParser(final String url)
			throws ParserException {
		final Parser htmlParser = new Parser(url);
		ArrayList<String> result = new ArrayList<>();

		try {
			final NodeList tagNodeList = htmlParser
					.extractAllNodesThatMatch(new NodeClassFilter(LinkTag.class));
			for (int j = 0; j < tagNodeList.size(); j++) {
				final LinkTag loopLink = (LinkTag) tagNodeList.elementAt(j);
				final String loopLinkStr = loopLink.getLink();
				result.add(loopLinkStr);
			}
		} catch (ParserException e) {
			e.printStackTrace(); // TODO handle error
		}
		return result;
	}
}