/**
 * 
 */
package lesson4.Task1_Parallel_Threads;

/**
 * GeekHub. Lesson4 
 * Task 4.1 (Find listing of files in a directory)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 10 Nov 2012. 
 * 
 * Find listing of files in a directory
 * with some pattern 
 * Using parallel threads
 */

import java.io.File;
import java.util.ArrayList;

public class ParallelFind implements Runnable {

	private int numThreads;  // number of threads
	private String pattern;  // given pattern of file
	private String path;     // path to directory
	private int threadNum;   // current number of thread
	
	private ArrayList<String> fileList = new ArrayList<>();  //local list for saving files listing
	private int count = 0;   // count the number of found file by local thread

	// Consrtructor 1
	public ParallelFind() {

	}

	// Consrtructor 2
	public ParallelFind(String pattern, String path, int threadNum,
			int numThreads) {
		this.pattern = pattern;
		this.path = path;
		this.numThreads = numThreads;
		this.threadNum = threadNum;
	}

	/**
	 *  Parallel files-list Find method
	 */
	public void run() {

		String files;
		pattern = pattern.substring(1); // Use pattern of file
		File folder = new File(path); // Use directory path
		File[] listOfFiles = folder.listFiles(); // get all files list in
													// directory
		int N = listOfFiles.length; // number of files in a folder

		// give each threads its own slice of data
		int from = threadNum * N / numThreads;
		int to = ((threadNum + 1) * N / numThreads);

		// find files with pattern and print
		for (int i = from; i < to; i++) {

			if (listOfFiles[i].isFile()) {
				files = listOfFiles[i].getName();
				if (files.endsWith(pattern.toLowerCase())
						|| files.endsWith(pattern.toUpperCase())) {
					fileList.add(files);
					count++;
				}
			}
		}
	}
	
	// local list of files
	public ArrayList<String> getFileList() {

		return fileList;
	}

	// number of files find by thread
	public int getNumFile() {

		return count;
	}
	
    // print local list of files
	public void print() {

		for (String s : fileList)
			System.out.println(s);
	}

}
