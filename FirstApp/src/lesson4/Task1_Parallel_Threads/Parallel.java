/**
 * 
 */
package lesson4.Task1_Parallel_Threads;

import java.util.ArrayList;

/**
 * GeekHub. Lesson4 
 * Task 4.1 (Parallel file finding)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 10 Nov 2012. 
 */
public class Parallel {

	static ArrayList<String> list = new ArrayList<>(); //share list containing all found files
	
	/**
	 * @param args
	 * 
	 *  Print listing of files in a directory
	 *  with some pattern in a name
	 */
	public static void main(String[] args){
		
		double start;
		double finish;
		int nThreads = 6;    //number of threads

		if (args.length <= 1) {
			System.out
					.println("Error! Use pattern (example: *.txt, *.java), 'directory path'");
		} else{
			start = System.nanoTime();
			find(args, nThreads);
			finish = System.nanoTime();
			
			printFileList();
			
			System.out.println("Time is: "+(finish-start)/10e6+"ms");
		}

	}
	
	/* 
	 * Find files with given pattern using multithreading
	 */
	public static void find(String[] args, int n) {

		ParallelFind[] f = new ParallelFind[n];
		Thread[] t = new Thread[n];

		for (int i = 0; i < n; i++) {

			f[i] = new ParallelFind(args[0], args[1], i, n);
			t[i] = new Thread(f[i]);
			t[i].start();

		}

		//synchronize all threads to print the list of files
		for (int i = 0; i < n; i++) {
			try {
				t[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

		// add to share list all local lists of files that was found by each of
		// thread
		for (int i = 0; i < n; i++)
			list.addAll(f[i].getFileList());
	}

	//print the list of files
	public static void printFileList() {
		for (String s : list)
			System.out.println(s);
	}
		
}
