/**
 * 
 */
package lesson4.Task3_Socket;

/**
 * GeekHub. Lesson4 
 * Task 4.3 (Work of Simple Server using socket)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 11 Nov 2012. 
 * 
 */

import java.io.*;
import java.net.*;

public class SimpleServer {

	public static void main(String[] args) throws IOException {

		System.out.println("Welcome to Server side");

		// Initialization
		BufferedReader in = null;
		PrintWriter out = null;
		ServerSocket servers = null;
		Socket fromclient = null;

		// create server socket
		int port = 3333;
		String input;
		try {
			servers = new ServerSocket(port);
		} catch (IOException e) {
			System.out.println("Couldn't listen to port " + port);
			System.exit(-1);
		}

		// Accept client
		fromclient = clientAccept(servers, fromclient);

		in = new BufferedReader(new InputStreamReader(fromclient.getInputStream()));
		out = new PrintWriter(fromclient.getOutputStream(), true);

		// Wait for message and answer to client
		System.out.println("Wait for messages:");
		while ((input = in.readLine()) != null) {
			if (input.equalsIgnoreCase("exit")) {
				System.out.println("Client disconected!");
				break;
			}
			out.println(input.charAt(0));
			System.out.println(input);
		}

		// close server socket
		out.close();
		in.close();
		fromclient.close();
		servers.close();
	}

	/**
	 * Method for accepting to client
	 */
	private static Socket clientAccept(ServerSocket servers, Socket fromclient) {
		try {
			System.out.print("Waiting for a client...");
			fromclient = servers.accept();
			System.out.println("Client connected");
		} catch (IOException e) {
			System.out.println("Can't accept");
			System.exit(-1);
		}
		return fromclient;
	}
}