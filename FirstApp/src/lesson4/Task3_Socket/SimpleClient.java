/**
 * 
 */
package lesson4.Task3_Socket;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * GeekHub. Lesson4 
 * Task 4.3 (Work of Simple Client using socket)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 11 Nov 2012. 
 * 
 * This program use a socket to communicate with a Simple
 * server.
 */
public class SimpleClient {

	public static void main(String[] args) {

		String host = "localhost";
		final int HTTP_PORT = 3333;

		// initialization
		Socket s = null;
		BufferedReader in = null;
		PrintWriter out = null;
		BufferedReader inu = null;

		try {
			System.out.println("Connecting to... " + host);

			// Open socket connection
			s = new Socket(host, HTTP_PORT);

			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new PrintWriter(s.getOutputStream(), true);
			inu = new BufferedReader(new InputStreamReader(System.in));

			String sUser;
			String sServer;

			// Send message to server and receive an answer
			while ((sUser = inu.readLine()) != null) {
				out.println(sUser);
				sServer = in.readLine();
				System.out.println(sServer);
				if (sUser.equalsIgnoreCase("exit"))
					break;
			}

		} catch (UnknownHostException uhe) {
			System.out.println("UnknownHostException: " + uhe);
		} catch (IOException e) {
			System.err.println("IOException: " + e);
		} finally {
			// Always close the socket at the end
			try {
				inu.close();
				in.close();
				out.close();
				s.close();
			} catch (IOException e) {
				System.out.println("Can not close streams..." + e.getMessage());
			}
		}
	}
}