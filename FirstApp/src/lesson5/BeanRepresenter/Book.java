package lesson5.BeanRepresenter;

/**
 * GeekHub. Lesson 5 
 * Task 5.1 (Represent class hierarchy of object
 * with its all fields and methods)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 18 Nov 2012.
 * 
 *  Additional class Book 
 */

public class Book {

	private String name;
	private int pages;
	private String writer;
	private int year;
	
	public Book(){
		
	}
	
	/**
	 * @param name - Name of the book 
	 * @param pages - Book size in pages
	 * @param writer - Name of the writer, author of the book 
	 * @param year - release date (only year)
	 */
	public Book(String name, int pages, String writer, int year) {
		this.name = name;
		this.pages = pages;
		this.writer = writer;
		this.year = year;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPages() {
		return pages;
	}

	public void setPages(int pages) {
		this.pages = pages;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}
	

}
