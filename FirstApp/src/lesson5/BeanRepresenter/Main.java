/**
 * 
 */
package lesson5.BeanRepresenter;

/**
 * GeekHub. Lesson 5 
 * Task 5.1 (Represent class hierarchy of object
 * with its all fields and methods)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 18 Nov 2012. 
 */

public class Main {

	public static void main(String[] args) {

		Book book = new Book("Northern lights", 607, "Philip Pullman", 1995);
		Song song = new Song("Skyfall", "Adele", "Skyfall", 2012);
		Human human = new Human("Tom", 19, book, song);

		BeanRepresenter bean = new BeanRepresenter();
		
		bean.print(human);
		bean.printFieldClass(human, "\t");
		bean.printMethodClass(human, "\t");
	}

}
