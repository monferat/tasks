package lesson5.BeanRepresenter;

/**
 * GeekHub. Lesson 5 
 * Task 5.1 (Represent class hierarchy of object
 * with its all fields and methods)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 18 Nov 2012.
 * 
 *  Additional class Song
 */
public class Song {

	private String name;
	private String artist;
	private String album;
	private int date;
	
	public Song(){
		
	}
	
	/**
	 * @param name - Song name
	 * @param artist - Artist name
	 * @param album - Album name
	 * @param date - release date(only year)
	 */
	public Song(String name, String artist, String album, int date) {
		this.name = name;
		this.artist = artist;
		this.album = album;
		this.date = date;
	}
	
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getAlbum() {
		return album;
	}
	public void setAlbum(String album) {
		this.album = album;
	}
	public int getDate() {
		return date;
	}
	public void setDate(int date) {
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

	
}
