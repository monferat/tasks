package lesson5.BeanRepresenter;

/**
 * GeekHub. Lesson 5 
 * Task 5.1 (Represent class hierarchy of object
 * with its all fields and methods)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 18 Nov 2012.
 * 
 *  Additional class Human
 */

public class Human {

	private String name;
	private int age;
	public Song track;
	public Book book;
	
	/**
	 * @param name
	 * @param age
	 * @param book
	 * @param track
	 */
	public Human(String name, int age, Book book, Song track) {
		this.name = name;
		this.age = age;
		this.track = track;
		this.book = book;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
}
