package lesson5.BeanRepresenter;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

/**
 * GeekHub. Lesson 5 
 * Task 5.1 (Represent class hierarchy of object
 * with its all fields and methods)
 *  
 * @author Vladimir Bezpalchuck
 *
 * 18 Nov 2012. 
 */

public class BeanRepresenter {
	
	/**
     * Print all fields of object
     */
    @SuppressWarnings("rawtypes")
	public void printFieldClass(Object obj, String s){
        
        Class clazz = obj.getClass();
        String nameClass = clazz.getSimpleName();
        Field[] fields = clazz.getDeclaredFields();
        System.out.println(s + nameClass);
        Field.setAccessible(fields, true);
        s += "\t"; 
        
		try {
			for (Field field : fields) {
				if (!field.getType().isInstance(toString()) && !field.getType().isPrimitive()) {
					printFieldClass(field.get(obj), s);
				} else {
					System.out.println(s + getModifiers(field.getModifiers())
							+ getType(field.getType()) + "\t" + field.getName()
							+ "\t" + field.get(obj));
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			System.out.println("Error! " + e.getLocalizedMessage());
		}
    }
    
    /**
     * Print all methods of object with annotations
     */
    @SuppressWarnings("rawtypes")
	public void printMethodClass(Object obj, String s) {

		Class clazz = obj.getClass();
		String nameClass = clazz.getSimpleName();
		Method[] methods = clazz.getDeclaredMethods();
		System.out.println("\n" + nameClass + " methods:");
		Method.setAccessible(methods, true);

		try {
			for (Method m : methods) {
				Annotation[] annotations = m.getAnnotations();
				System.out.print(s);
				for (Annotation a : annotations)
					System.out.print("@" + a.annotationType().getSimpleName() + " ");
				System.out.println();
				System.out.print(s + getModifiers(m.getModifiers())
						+ getType(m.getReturnType()) + " " + m.getName() + "(");
				System.out.print(getParameters(m.getParameterTypes()));
				System.out.println(") { }");
			}
		} catch (IllegalArgumentException e) {
			System.out.println("Error! " + e.getLocalizedMessage());
		}
	}
    
    /**
     * Print class hierarchy of object
     */
    @SuppressWarnings("rawtypes")
	public void printClasses( Class type, String s ) {
    	 
        // Recurse super classes
        if ( type.getSuperclass() != null ){ 
        // true for all except java.lang.Object
           printClasses( type.getSuperclass(), s);
        }
        s += "\t"; 
        // Print class name
        System.out.print(type.getSimpleName()+"\n"+s);
       
     }
    
    //Print class hierarchy
    @SuppressWarnings("rawtypes")
	public void print(Object obj){
    	 Class type = obj.getClass();
    	 this.printClasses(type, "");
    }
    
    /**
     * Get modifiers of fields or methods
     */
    static String getModifiers(int m) {
        String modifiers = "";
        if (Modifier.isPublic(m)) modifiers += "public ";
        if (Modifier.isProtected(m)) modifiers += "protected ";
        if (Modifier.isPrivate(m)) modifiers += "private ";
        if (Modifier.isStatic(m)) modifiers += "static ";
        if (Modifier.isAbstract(m)) modifiers += "abstract ";
        return modifiers;
    }

    /* 
     * check and get type of fields or methods
     */
    @SuppressWarnings("rawtypes")
	static String getType(Class clazz) {
        String type = clazz.isArray()
                ? clazz.getComponentType().getSimpleName()
                : clazz.getSimpleName();
        if (clazz.isArray()) type += "[]";
        return type;
    }
    
    /**
     * Get parameters of methods
     */
    @SuppressWarnings("rawtypes")
	static String getParameters(Class[] params) {
        String p = "";
        for (int i = 0, size = params.length; i < size; i++) {
            if (i > 0) p += ", ";
            p += getType(params[i]) + " p" + i;
        }
        return p;
    }

}
