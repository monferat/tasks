package com.geekhub.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "FRIENDSHIP")
public class Friendship {

	@GeneratedValue
	@Id
	@Column(name = "ID")
	private Integer id;

	@ManyToOne
	@JoinColumn(name = "FRIENDID_1")
	@Index(name = "friend_1_Idx")
	private User friendId_1;

	@ManyToOne
	@JoinColumn(name = "FRIENDID_2")
	@Index(name = "frined_2_Idx")
	private User friendId_2;

	@Column(name = "STATUS")
	private byte status;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public User getFriendId_1() {
		return friendId_1;
	}

	public void setFriendId_1(User friendId_1) {
		this.friendId_1 = friendId_1;
	}

	public User getFriendId_2() {
		return friendId_2;
	}

	public void setFriendId_2(User friendId_2) {
		this.friendId_2 = friendId_2;
	}

	public byte getStatus() {
		return status;
	}

	public void setStatus(byte status) {
		this.status = status;
	}

}