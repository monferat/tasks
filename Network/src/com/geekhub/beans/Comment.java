package com.geekhub.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.lang.Integer;
import java.util.Date;


@Entity
@Table(name="COMMENT")
public class Comment {
	
	@GeneratedValue
	@Id
	@Column(name="ID")
	private Integer id;
	
	@Column(name="TITLE")
    private String title;
    
	@Column(name="DESCRIPTION")
    private String description;
	
	@ManyToOne
	@JoinColumn(name="OWNERID")
    private User ownerId;
	
	@Column(name="TDATE")
    private Date tdate = new Date();
	
	@ManyToOne
	@JoinColumn(name="RECEIVER")
    private User receiver;
	
	@Column(name="FLAG")
    private Byte flag = 0;  // flag = 0 - post message, flag = 1 - dialog message							
							// flag = other - comment message
	
    public Byte getFlag() {
		return flag;
	}

	public void setFlag(Byte flag) {
		this.flag = flag;
	}

	public User getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(User ownerId) {
		this.ownerId = ownerId;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public Date getTdate() {
		return tdate;
	}

	public void setTdate(Date tdate) {
		this.tdate = tdate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
    public User getOwner() {
        return ownerId;
    }
    
    public void setOwner(User ownerId) {
        this.ownerId = ownerId;
    }
}
