package com.geekhub.beans;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PHOTOALBUM")
public class Photo {
	
	@GeneratedValue
	@Id
	@Column(name="ID")
	private Integer id;
	
	@Column(name="PHOTO_TITLE")
    private String photo_title;
    
	@Column(name="PHOTO_DESCRIPTION")
    private String photo_description;
	
	@ManyToOne
	@JoinColumn(name="OWNERID")
    private User ownerId;
	
	@Column(name="FDATE")
    private Date tdate = new Date();

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getPhoto_title() {
		return photo_title;
	}

	public void setPhoto_title(String photo_title) {
		this.photo_title = photo_title;
	}

	public String getPhoto_description() {
		return photo_description;
	}

	public void setPhoto_description(String photo_description) {
		this.photo_description = photo_description;
	}

	public User getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(User ownerId) {
		this.ownerId = ownerId;
	}

	public Date getTdate() {
		return tdate;
	}
	

	
}	
