package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.geekhub.beans.Photo;
import com.geekhub.beans.User;
import com.geekhub.services.PhotoDAO;
import com.geekhub.services.UserDAO;

@Controller
public class PhotoController {
	
  @Autowired PhotoDAO photoDAO;
  @Autowired UserDAO userDAO;

	@RequestMapping(value = "upload_photo.html")
	public String uploadPhoto(
			@RequestParam(value = "file") MultipartFile multipartFile,
			HttpSession session,
			@RequestParam(value = "receiverId") Integer receiverId,
			ModelMap map, HttpServletRequest request) {

		User user = (User) session.getAttribute("loggedUser");		
		String resorceFolder = request.getSession().getServletContext().getInitParameter("MyRes");
		String orgName = multipartFile.getOriginalFilename();
		String filePlaceToUpload = resorceFolder + "album/" + user.getId() +"/";		
		String contentPath = "album/" + user.getId() +"/" + orgName;

		map.put("id", receiverId);
				
		Photo photo = new Photo();
		photo.setPhoto_title(contentPath);
		photo.setOwnerId(user);
		
		File userFolder = new File(filePlaceToUpload);
		if(!userFolder.exists()){
			userFolder.mkdir();
		}
		String filePath = filePlaceToUpload + orgName;
		File dest = new File(filePath);
		try {
			multipartFile.transferTo(dest);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
			return "File uploaded failed:" + orgName;
		}

		photoDAO.savePhoto(photo);

		return "redirect:photo_album.html";
	}

  @RequestMapping("/photo_album.html")
	public String photoPage(
			@RequestParam(value = "id", required = false) Integer id,
			HttpSession session, ModelMap map, HttpServletRequest request) {
		
		String resorceFolder = request.getSession().getServletContext().getInitParameter("MyRes");
		User user = (User) session.getAttribute("loggedUser");	
		map.put("user", user);

		map.put("id", id);

		User gettedUser = userDAO.getUserById(id);
		map.put("getedUser", gettedUser);
		map.addAttribute("username", gettedUser.getName());
		map.addAttribute("resPath", resorceFolder);

		List<Photo> photos = photoDAO.getPhotoListbyOwner(id);
		map.put("photos", photos);

		return "photo_album";
	}
  
}
