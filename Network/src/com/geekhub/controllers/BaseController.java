package com.geekhub.controllers;

import java.security.Principal;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.geekhub.beans.User;
import com.geekhub.services.CommentDAO;
import com.geekhub.services.UserDAO;


@Controller
public class BaseController {
	 
	@Autowired UserDAO userDAO;
	@Autowired CommentDAO commentDAO;
	
	@RequestMapping(value="index.html")
	public String index(HttpSession session, ModelMap map, Principal principal) {
				
		String name = principal.getName();
		map.addAttribute("username", name);		
		
		User user  = userDAO.getUserByLogin(name);
		map.put("user", user);
		String ava=user.getAvatar();
		if (ava != null){ 
			map.addAttribute("avatar", ava);
		}
		session.setAttribute("loggedUser", user);
		return "redirect:./getPage.html?id="+user.getId();
	}
	
	@RequestMapping(value="login.action")
	public String login (ModelMap map) {
		
		return "login";
	}
  
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logout(ModelMap map) {
 
		return "login";
 
	}
	
}
