package com.geekhub.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import com.geekhub.beans.Comment;
import com.geekhub.beans.User;
import com.geekhub.services.CommentDAO;
import com.geekhub.services.UserDAO;

@Controller
public class PostController {

	@Autowired
	UserDAO userDAO;
	@Autowired
	CommentDAO commentDAO;

	@RequestMapping(value = "message.action")
	public @ResponseBody
	String getMessage(@RequestParam(value = "name") String name,
			@RequestParam(value = "description") String description,
			@RequestParam(value = "ownerId") Integer ownerId,
			@RequestParam(value = "receiverId") Integer receiverId,
			@RequestParam(value = "flag") Byte flag, ModelMap map)
			throws ParseException {

		String result = new Date().toString();

		map.put("id", receiverId);
		if (description != null) {
			Comment comment = new Comment();
			comment.setDescription(description);
			comment.setTitle(name);
			comment.setOwner(userDAO.getUserById(ownerId));
			comment.setReceiver(userDAO.getUserById(receiverId));
			comment.setFlag(flag);
			commentDAO.saveComment(comment);
		}
		return result;
	}

	@RequestMapping(value = "/post.html")
	public String getPost(
			@RequestParam(value = "id", required = false) Integer id,
			HttpSession session, ModelMap map) {

		User user = (User) session.getAttribute("loggedUser");
		map.put("user", user);

		List<Comment> comments = commentDAO.getCommentListbyReceiver(id);

		if (!comments.isEmpty()) {
			map.put("comments", comments);
		}
		
		map.put("id", id);

		User targetUser = userDAO.getUserById(id);
		map.put("targetUser", targetUser);

		map.addAttribute("username", targetUser.getName());
		String ava = targetUser.getAvatar();
		if (ava != null) {
			map.addAttribute("avatar", ava);
		}

		return "post";
	}
	
	@RequestMapping(value = "/messages.html")
	public String getDialogMessage(
			@RequestParam(value = "id", required = false) Integer id,
			HttpSession session, ModelMap map) {

		User user = (User) session.getAttribute("loggedUser");
		map.put("user", user);

		List<Comment> comments = commentDAO.getCommentListDialog(user.getId(), id);
		
		if (!comments.isEmpty()) {
			map.put("comments", comments);
		}
		
		map.put("id", id);
		User targetUser = userDAO.getUserById(id);
		map.put("targetUser", targetUser);

		map.addAttribute("username", targetUser.getName());
		String ava = targetUser.getAvatar();
		if (ava != null) {
			map.addAttribute("avatar", ava);
		}

		return "messages";
	}

}
