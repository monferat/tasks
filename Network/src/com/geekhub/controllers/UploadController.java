package com.geekhub.controllers;

import java.io.File;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.geekhub.beans.User;
import com.geekhub.services.UserDAO;

@Controller
public class UploadController {

	@Autowired
	UserDAO userDAO;

	@RequestMapping(value = "chAvatar.html")
	public String load(
			@RequestParam(value = "id", required = false) Integer id,
			ModelMap map) {

		User user = userDAO.getUserById(id);
		map.put("user", user);
		String ava = user.getAvatar();
		if (ava != null) {
			map.addAttribute("avatar", ava);
		}
		map.addAttribute("id", id);
		return "chAvatar";
	}

	@RequestMapping(value = "/save_uploaded_file.html")
	public String handleUpload(
			@RequestParam(value = "file") MultipartFile multipartFile,
			@RequestParam(value = "id", required = false) Integer id,
			HttpServletRequest request) {

		String resorceFolder = request.getSession().getServletContext()
				.getInitParameter("MyRes");

		String orgName = multipartFile.getOriginalFilename();
		String filePlaceToUpload = resorceFolder + "res/" + id +"/";
		File userFolder = new File(filePlaceToUpload);
		if(!userFolder.exists()){
			userFolder.mkdir();
		}
		String filePath = filePlaceToUpload + orgName;
		String contentPath = "res/" + id +"/" + orgName;
		File dest = new File(filePath);
		try {
			multipartFile.transferTo(dest);
		} catch (IllegalStateException | IOException e) {
			e.printStackTrace();
			return "File uploaded failed:" + orgName;
		}

		userDAO.saveAvatar(contentPath, id);

		return "redirect:./chAvatar.html?id=" + id;
	}

}
