package com.geekhub.controllers;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.geekhub.beans.Comment;
import com.geekhub.beans.User;
import com.geekhub.services.CommentDAO;
import com.geekhub.services.FriendshipDAO;
import com.geekhub.services.UserDAO;

@Controller
public class CommentController {

	@Autowired
	CommentDAO commentDAO;
	@Autowired
	UserDAO userDAO;
	@Autowired
	FriendshipDAO friendshipDAO;

	@RequestMapping(value = "./admin/listComments.html")
	public String list(ModelMap map) {
		map.put("comments", commentDAO.getCommentList());
		return "comments";
	}

	@RequestMapping(value = "loadComment.html")
	public String load(
			@RequestParam(value = "id", required = false) Integer id,
			ModelMap map) {
		Comment comment = id == null ? new Comment() : commentDAO
				.getCommentById(id);
		map.put("comment", comment);
		map.put("users", userDAO.getUserList());
		return "comment";
	}

	@RequestMapping(value = "deleteComment.action")
	public String delete(
			@RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "usrId", required = true) Integer userId) {
		commentDAO.deleteComment(id);
		return "redirect:post.html?id=" + userId;
	}

	@RequestMapping(value = "deleteCommentDialog.action")
	public String deleteDialog(
			@RequestParam(value = "id", required = true) Integer id,
			@RequestParam(value = "usrId", required = true) Integer userId) {
		commentDAO.deleteComment(id);
		return "redirect:messages.html?id=" + userId;
	}

	@RequestMapping(value = "saveComment.html")
	public String save(Comment comment) {
		commentDAO.saveComment(comment);
		return "redirect:listComments.html";
	}
	
	@RequestMapping(value = "getMyMessages.html")
	public String getMyMessages(@RequestParam(value = "id", required = false) Integer id, ModelMap map) {				
				
		Set<User> myMessangers = new HashSet<User>();
		List<Comment> userDialogs = commentDAO.getAllCommentListDialog(id);
		for (Comment tmp : userDialogs) {
			if (tmp.getOwnerId().getId() != id) {
				myMessangers.add(tmp.getOwnerId());
			}
			else{
				myMessangers.add(tmp.getReceiver());
			}
		}
		
		map.put("myMessangers", myMessangers);				
		
		return "myMessages";
	}
}
