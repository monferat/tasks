package com.geekhub.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geekhub.beans.Friendship;
import com.geekhub.beans.User;
import com.geekhub.services.CommentDAO;
import com.geekhub.services.FriendshipDAO;
import com.geekhub.services.UserDAO;

@Controller
public class UserController {
	
	@Autowired UserDAO userDAO;
	@Autowired CommentDAO commentDAO;
	@Autowired FriendshipDAO friendshipDAO;
	
	@RequestMapping(value = "/getPage.html")
	public String getUserPage(ModelMap map, HttpSession session,
			@RequestParam(value = "id", required = false) Integer id) {
		User user = userDAO.getUserById(id);
		map.put("targetUser", user);
		User loggedUser = (User) session.getAttribute("loggedUser");
		map.put("loggedUser", loggedUser);
		Boolean check = friendshipDAO.friendCheck(id, loggedUser.getId());
		map.addAttribute("check", check);
		String ava = user.getAvatar();
		if (ava != null) {
			map.addAttribute("avatar", ava);
		}
		return "index";
	}
	
	@RequestMapping(value="./admin/listUsers.html")
	public String list(ModelMap map) {
		map.put("users",userDAO.getUserList());
		return "users";
	}
	
	@RequestMapping(value="loadUser.html")
	public String load(@RequestParam(value="id", required=false) Integer id, ModelMap map) {
		User user = id == null ? new User() : userDAO.getUserById(id);
		map.put("user", user);
		return "user";
	}
	
	@RequestMapping(value="deleteUser.html")
	public String delete(@RequestParam(value="id", required=true) Integer id) {
		userDAO.deleteUser(id);
		return "redirect:listUsers.html";
	}
	
	@RequestMapping(value = "saveUser.html")
	public String save(User user,
			@RequestParam(value = "date", required = false) String date)
			throws ParseException {
		if (date != "") {
			SimpleDateFormat formatDate = new SimpleDateFormat("MM/dd/yyyy");
			Date birthday = formatDate.parse(date);
			user.setBirthdate(birthday);
		} else {
			user.setBirthdate(user.getBirthdate());
		}
		
		String password = user.getPassword();
        user.setPassword(DigestUtils.md5Hex(password));

		userDAO.saveUser(user);
		return "redirect:index.html";
	}
	

	@RequestMapping(value = "checkLogin.html")
	public @ResponseBody
	String checkLogin(
			@RequestParam(value = "login", required = true) String login) {
		if (userDAO.loginExist(login)) {
			return "false";
		} else
			return "true";
	}

	@RequestMapping(value = "/searchUser.html")
	public String find(ModelMap map, User user, HttpSession session) {
		
		User loggedUser = (User)session.getAttribute("loggedUser");        
        map.put("loggedUser", loggedUser);
        
		if (user == null) {
			user = new User();
			map.put("user", user);
			return "search";
		}

		List<User> users = userDAO.findUsers(user);		
		Map<User, Boolean> mapUsers = new HashMap<User, Boolean>();
		Boolean check = false;
		for (User usr : users) {
			check = friendshipDAO.friendCheck(usr.getId(), loggedUser.getId());
			mapUsers.put(usr, check);
		}	
		map.put("mapUsers", mapUsers);
		
		return "search";

	}
	
	@RequestMapping(value = "/addFriend.html")
	public String addFriend(ModelMap map,
			@RequestParam(value = "userId", required = false) Integer userId, HttpSession session) {

		User user1 = (User) session.getAttribute("loggedUser");	
		User user2 = userDAO.getUserById(userId);		
		Friendship fs;

		fs = friendshipDAO.getNotConfemedFriends(user1, user2);
		if (fs != null) {
			fs.setStatus((byte) 1);
			friendshipDAO.save(fs);
		} else {
			fs = new Friendship();
			fs.setFriendId_1(user1);
			fs.setFriendId_2(user2);
			friendshipDAO.save(fs);
		}
		return "redirect:./getPage.html?id=" + userId;

	}

	@RequestMapping(value = "/deleteFriend.html")
	public String deleteFriend(
			ModelMap map,
			@RequestParam(value = "userId1", required = false) Integer userId_1,
			@RequestParam(value = "userId2", required = false) Integer userId_2) {

		friendshipDAO.deleteMyFriend(userId_1, userId_2);

		return "redirect:./getFriends.html?id=" + userId_2;
	}

	@RequestMapping(value = "/getFriends.html")
	public String getFriend(ModelMap map,
			@RequestParam(value = "id", required = true) Integer id, HttpSession session) {

		User user = userDAO.getUserById(id);
		User loggedUser = (User) session.getAttribute("loggedUser");	
		List<Friendship> friends = friendshipDAO.getFriends(user);
		List<Friendship> friendsRequest = friendshipDAO
				.getFriendsRequests(loggedUser);
		List<User> reqFriends = new ArrayList<User>();
		List<User> myFriends = new ArrayList<User>();
		for (Friendship tmp : friends) {
			if (tmp.getStatus() != 0) {
				if (tmp.getFriendId_1().getId() != user.getId())
					myFriends.add(tmp.getFriendId_1());
				else
					myFriends.add(tmp.getFriendId_2());
			}
		}
		for (Friendship rf : friendsRequest) {
			reqFriends.add(rf.getFriendId_1());
		}
		map.addAttribute("requestCount", friendsRequest.size());
		map.put("friendsRequest", reqFriends);
		map.put("myFriends", myFriends);
		map.put("targetUser", user);
		map.put("loggedUser", loggedUser);
		return "friends";
	}
	
	@RequestMapping(value = "addFriendRequest.html", method = RequestMethod.POST)
	public @ResponseBody
	String addFriendRequest(@RequestParam(value = "userId") Integer userId, HttpSession session) {
		
		User user1 = (User) session.getAttribute("loggedUser");	
		User user2 = userDAO.getUserById(userId);		
		Friendship fs;

		fs = friendshipDAO.getNotConfemedFriends(user1, user2);
		if (fs != null) {
			fs.setStatus((byte) 1);
			friendshipDAO.save(fs);
		} else {
			fs = new Friendship();
			fs.setFriendId_1(user1);
			fs.setFriendId_2(user2);
			friendshipDAO.save(fs);
		}

		return "Request sent";
	}
	
	@RequestMapping(value = "addFriendRequest.html", method = RequestMethod.GET)
	public @ResponseBody
	String showSearch() {		
		return "search";
	}
	
	
}
