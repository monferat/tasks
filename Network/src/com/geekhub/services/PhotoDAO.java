package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Photo;

public interface PhotoDAO {
	
	public List<Photo> getPhotoList();
	
	public Photo getPhotoById(Integer id);
	
	public void deletePhoto(Integer id);
	
	public void savePhoto(Photo photo);
	
	public List<Photo> getPhotoListbyOwner(Integer ownerId);
	
}
