package com.geekhub.services;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.User;

@Repository
@Transactional
public class UserDAOImpl implements UserDAO {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<User> getUserList() {
		return sessionFactory.getCurrentSession().createQuery("from User")
				.list();
	}

	@Override
	public User getUserById(Integer id) {
		return (User) sessionFactory.getCurrentSession().get(User.class, id);
	}

	@Override
	public User getUserByLogin(String login) {
		return (User) sessionFactory.getCurrentSession()
				.createQuery("from User where login=:login")
				.setParameter("login", login).uniqueResult();
	}

	@Override
	public void deleteUser(Integer id) {
		User user = getUserById(id);
		if (null != user)
			sessionFactory.getCurrentSession().delete(user);

	}

	@Override
	public void saveUser(User user) {
		sessionFactory.getCurrentSession().saveOrUpdate(user);

	}

	@Override
	public boolean loginExist(String login) {
		return ((User) sessionFactory.getCurrentSession()
				.createQuery("from User where login=:login")
				.setParameter("login", login).uniqueResult() != null);
	}

	@Override
	public void saveAvatar(String avatar, Integer id) {
		User user = getUserById(id);
		user.setAvatar(avatar);
		if (null != user)
			sessionFactory.getCurrentSession().saveOrUpdate(user);

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> findUsers(User user) {
		Criteria criteria = sessionFactory.getCurrentSession()
				.createCriteria(User.class)
				.add(Restrictions.like("name", user.getName() + "%"))
				.add(Restrictions.like("login", user.getLogin() + "%"))
				.add(Restrictions.like("surname", user.getSurname() + "%"))
				.add(Restrictions.like("country", user.getCountry() + "%"))
				.add(Restrictions.like("email", user.getEmail() + "%"));
		return criteria.list();
	}

}
