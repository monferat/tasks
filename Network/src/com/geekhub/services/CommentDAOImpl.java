package com.geekhub.services;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Comment;

@Repository
@Transactional
public class CommentDAOImpl implements CommentDAO{
	
	@Autowired SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> getCommentList() {
		return sessionFactory.getCurrentSession().createQuery("from Comment").list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> getCommentListbyOwner(Integer Id) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("from Comment where ownerId.id=:Id")
				.setParameter("Id", Id);
		return query.list();
	}

	@Override
	public Comment getCommentById(Integer id) {
		return (Comment) sessionFactory.getCurrentSession().get(Comment.class, id);
	}

	@Override
	public void deleteComment(Integer id) {
		Comment comment = getCommentById(id);
		if(null != comment)
			sessionFactory.getCurrentSession().delete(comment);
	}

	@Override
	public void saveComment(Comment comment) {
		sessionFactory.getCurrentSession().saveOrUpdate(comment);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> getCommentListbyReceiver(Integer receiverId) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("from Comment where receiver.id=:receiverId")
				.setParameter("receiverId", receiverId);
		return query.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Comment> getCommentListDialog(Integer ownerId,
			Integer receiverId) {
		return sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Comment where ((receiver=? and ownerId=?) OR (receiver=? and ownerId=?)) AND FLAG=1")
				.setInteger(0, receiverId).setInteger(1, ownerId).setInteger(2, ownerId).setInteger(3, receiverId).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<Comment> getAllCommentListDialog(Integer ownerId) {
		return sessionFactory
				.getCurrentSession()
				.createQuery("from Comment where (receiver=? OR ownerId=?) AND FLAG=1")
				.setInteger(0, ownerId).setInteger(1, ownerId).list();
	}

}
