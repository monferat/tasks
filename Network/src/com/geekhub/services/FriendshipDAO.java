package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Friendship;
import com.geekhub.beans.User;

public interface FriendshipDAO {

	void save(Friendship fr);

	public void deleteMyFriend(Integer friendid_1, Integer friendid_2);

	public List<Friendship> getFriends(User user);

	Friendship getNotConfemedFriends(User user1, User user2);
	
	public List<Friendship> getFriendsRequests(User user);
	
	public Boolean friendCheck(Integer targetId, Integer userId); 
	
	public List<Friendship> getAllFriendshipList();

}
