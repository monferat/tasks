package com.geekhub.services;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Friendship;
import com.geekhub.beans.User;

@Repository
@Transactional
public class FriendshipDAOImpl implements FriendshipDAO {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Friendship> getAllFriendshipList() {
		return sessionFactory.getCurrentSession().createQuery("from Friendship")
				.list();
	}
	
	@Override
	public void save(Friendship fr) {
		sessionFactory.getCurrentSession().saveOrUpdate(fr);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Friendship> getFriends(User user) {
		return sessionFactory
				.getCurrentSession()
				.createQuery("from Friendship where (FRIENDID_1 = ? OR FRIENDID_2 =?) AND status=1")
				.setInteger(0, user.getId()).setInteger(1, user.getId()).list();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Friendship> getFriendsRequests(User user) {
		return sessionFactory
				.getCurrentSession()
				.createQuery("from Friendship where (FRIENDID_2 =?) AND status=0")
				.setInteger(0, user.getId()).list();
	}


	@Override
	public Friendship getNotConfemedFriends(User user1, User user2) {
		return (Friendship) sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Friendship where (FRIENDID_1 = ? AND FRIENDID_2 =?) OR (FRIENDID_1 = ? AND FRIENDID_2 =?) AND status=0")
				.setInteger(0, user1.getId()).setInteger(1, user2.getId())
				.setInteger(2, user2.getId()).setInteger(3, user1.getId())
				.uniqueResult();

	}

	@Override
	public void deleteMyFriend(Integer friendid_1, Integer friendid_2) {
		sessionFactory
				.getCurrentSession()
				.createQuery(
						"delete from Friendship where (friendid_1 = ? AND friendid_2 = ?) OR (friendid_1 = ? AND friendid_2 = ?)")
				.setInteger(0, friendid_1).setInteger(1, friendid_2)
				.setInteger(2, friendid_2).setInteger(3, friendid_1)
				.executeUpdate();

	}

	public Boolean friendCheck(Integer targetId, Integer userId) {
		return (sessionFactory
				.getCurrentSession()
				.createQuery(
						"from Friendship where ((FRIENDID_1 = ? AND FRIENDID_2 =?) OR (FRIENDID_1 = ? AND FRIENDID_2 =?)) AND status=1")
				.setInteger(0, targetId).setInteger(1, userId)
				.setInteger(2, userId).setInteger(3, targetId).list().size() != 0);
	}

}