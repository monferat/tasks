package com.geekhub.services;

import java.util.List;

import com.geekhub.beans.Comment;

public interface CommentDAO {
	
	public List<Comment> getCommentList();
	
	public Comment getCommentById(Integer id);
	
	public void deleteComment(Integer id);
	
	public void saveComment(Comment ticket);
	
	public List<Comment> getCommentListbyOwner(Integer ownerId);
	
	public List<Comment> getCommentListbyReceiver(Integer receiverId);
	
	public List<Comment> getCommentListDialog(Integer ownerId, Integer receiverId);
	
	public List<Comment> getAllCommentListDialog(Integer ownerId);
}
