package com.geekhub.services;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.geekhub.beans.Photo;

@Repository
@Transactional
public class PhotoDAOImpl implements PhotoDAO {

	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	@Override
	public List<Photo> getPhotoList() {
		return sessionFactory.getCurrentSession()
				.createQuery("from Photo").list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Photo> getPhotoListbyOwner(Integer Id) {
		Query query = sessionFactory.getCurrentSession()
				.createQuery("from Photo where ownerId.id=:Id")
				.setParameter("Id", Id);
		return query.list();
	}

	@Override
	public Photo getPhotoById(Integer id) {
		return (Photo) sessionFactory.getCurrentSession().get(
				Photo.class, id);
	}

	@Override
	public void deletePhoto(Integer id) {
		Photo photo = getPhotoById(id);
		if (null != photo)
			sessionFactory.getCurrentSession().delete(photo);
	}

	@Override
	public void savePhoto(Photo photo) {
		sessionFactory.getCurrentSession().saveOrUpdate(photo);
	}

}
