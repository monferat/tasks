function checkLogin() {
        if (!$("#login").valid())
                return false;
        var login = $('#login').val();
        var contexPath = '/Bugtracker_Hibernate/checkLogin.html';

        $.ajax({
                type : "POST",
                url : contexPath,
                data : "login=" + login,
                success : function(response) {
                        alert("Success");                      
                        if (response == "false") {
                                $('#submit').attr("disabled", "true");
                                $('#loginInfo').html("Such login already exists");
                        } else {
                                $('#submit').removeAttr("disabled");
                                $('#loginInfo').html("");
                        }
                },
                error : function(e) {
                        alert('Error: ' + e);
                }
        });
}