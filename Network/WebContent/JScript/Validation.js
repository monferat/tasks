	$(document).ready(function(){
				$("#form").validate({
					rules:{
						login:{
							required: true,
							minlength: 3
						},
						password:{
							required: true,
							minlength: 6
						},
						name: 'required'
					},
				    messages: {
				    	login: {
				    		required: " Login required",
				    		minlength: " Your Login must be 3 characters at least"
				   		},
				   		name: " Please specify your name",
				   		password:{
				   			required: " Password required",
				   			minlength: " Password must be 6 characters or more"
				   		}
					}
				});
				$("#form").validate();
			});