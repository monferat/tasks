<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
	   <title>CSS3 Form Table</title> 
	   <link rel="stylesheet" type="text/css" href="CSS/form_style.css">
	</head>
<body>
	<div class="CSSTable" >
		<table border="1">
		    <tr>
		        <th>Title</th>
		        <th>Description</th>
		        <th>OwnerId</th>
		        <th></th>
		        <th></th>
		    </tr>
		    <c:forEach items="${comments}" var="comment">
		        <tr>
		            <td>${comment.title}</td>
		            <td>${comment.description}</td>
		            <td>${comment.owner.id}</td>	
		       	            	            
		            <td><a href="./loadComment.html?id=${comment.id}">Edit</a></td>
		            <td style="color: red;"><a href="./deleteComment.action?id=${comment.id}">Delete</a></td>
		        </tr>
		    </c:forEach>
		</table>
	</div>
	
	<p><a href="./loadComment.html">Create New Comment</a></p>
	<p><a href="./index.html">Main Page</a></p>
</body>
</html>