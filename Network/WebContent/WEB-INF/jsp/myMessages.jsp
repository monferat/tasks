<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="JScript/jquery-1.9.1.js"></script>
<title>Edit info</title>
</head>
<body>
	<a href="./index.html">My Page(<sec:authentication property="principal.username" />)</a>

    <p>My Messages</p>
    <div class="CSSTable">
	<table border="2">
		<c:forEach items="${myMessangers}" var="messanger">
			<tr>																								
				<td><a href="./messages.html?id=${messanger.id}">Messages(<c:out value="${messanger.name}" /> <c:out value="${friend.surname}:" />)</a></td>											
			</tr>
		</c:forEach>
	</table>
	</div>
</body>
</html>