<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Photo Album</title>
  <link href="CSS/galleriffic.css" rel="stylesheet" type="text/css" />
 <link href="CSS/main.css" rel="stylesheet" type="text/css" />
 <link rel="stylesheet" href="CSS/programdiary.css" />
   <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
  <script type="text/javascript" src="JScript/jquery.history.js"></script>
  <script type="text/javascript" src="JScript/jquery.galleriffic.js"></script>
  <script type="text/javascript" src="JScript/jquery.opacityrollover.js"></script>
  <script type="text/javascript" src="JScript/main.js"></script>
</head>
<body>
<div id="main">
		<p><a href="./index.html">My Page</a></p>

		<h3>Username : ${user.name}</h3>
		<h3>Page Owner : ${getedUser.name}</h3>

		<form action="./upload_photo.html" method="post"
			enctype="multipart/form-data">
			<input type="file" name="file" accept="image/*,image/jpeg">
			<p>
				<input type="hidden" name="receiverId" value="${id}"> <input
					type="submit" value="Send" /> <input type="reset" />
			</p>
		</form>
		<a class="btn" href="<c:url value="/j_spring_security_logout" />">Logout</a><br/><br/><br/><br/>
		<p><h3>${getedUser.name}'s Album</h3></p>
</div>
	 <div class="container">	 
            <div class="navigation-container">
                <div id="thumbs" class="navigation">
                    <a class="pageLink prev" style="visibility: hidden;" href="#" title="Previous Page"></a>

                    <ul class="thumbs">
                    <c:forEach items="${photos}" var="photo">
                        <li>
                            <a class="thumb" name="photo${photo.id}" href="${pageContext.request.contextPath}/${photo.photo_title}" title="Title #1 ${photo.id}" >
                               <img src="${pageContext.request.contextPath}/${photo.photo_title}" width="50" height="50" alt="Title #1 ${photo.id}" />
                            </a>
                            <div class="caption">
                                <div class="image-title">Image ${photo.id}</div>
                                <div class="image-desc"><b>Description</b></div>                                
                            </div>
                        </li>      
                       </c:forEach>                             
                    </ul>
                    <a class="pageLink next" style="visibility: hidden;" href="#" title="Next Page"></a>
                </div>
            </div>
            <div class="content">
                <div class="slideshow-container">
                    <div id="controls" class="controls"></div>
                    <div id="loading" class="loader"></div>
                    <div id="slideshow" class="slideshow"></div>
                </div>
                <div id="caption" class="caption-container">
                    <div class="photo-index"></div>
                </div>
            </div>
            <div style="clear: both;"></div>
        </div>
</body>
</html>
