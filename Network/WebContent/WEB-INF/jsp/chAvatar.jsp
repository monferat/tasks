<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
	<head>
	<title>Avatar</title> 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
		<link rel="stylesheet" type="text/css" href="CSS/user_style.css">
		<script src="http://code.jquery.com/jquery-latest.js"></script>
	</head>
	<body>
	<p><a href="./index.html">My Page (<sec:authentication property="principal.username" />)</a></p>
	    <img src="${pageContext.request.contextPath}/${avatar}" align="left" width="255" height="255"  hspace="10" vspace="10" alt="lorem"/>	    
		<form class="form" action="./save_uploaded_file.html" method="post" enctype="multipart/form-data">
			<div><h1>Change Avatar:</h1><br></div>
			<p class="login"> <label for="login">Avatar:</label></p>	
			<input type="file" name="file" accept="image/*,image/jpeg">
			 <input type="hidden" name="id" value="${id}">	
			<p>
				<input class="btn" type="submit" value="Send" style="height: 25px; width: 100px" />
			    <input class="btn" type="reset" value="Reset" style="height: 25px; width: 100px"/>
			</p>
		</form>
	</body>	
</html>