<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="JScript/jquery-1.9.1.js"></script>

<title>Search</title>
<link rel="stylesheet" type="text/css" href="CSS/search.css" />
<script type="text/javascript">
	function doAjaxFriendRequestPost(id) {
		// get the form values
		var userId = id;		

		$.ajax({
			type : "POST",
			url : "./addFriendRequest.html",
			data : "userId=" + userId,
			success : function(response) {				
				$('#info'+userId).html(response);				
			},
			error : function(e) {
				alert('Error: ' + e);
			}
		});
	}
</script>
</head>

<body>
    <br/><center><a href="./index.html">My Page (<sec:authentication property="principal.username" />)</a><br/></center><br/>    	
    <center><h2>Search</h2></center>
    <hr size="2" />
    <div class="container">
	 <section id="content">
        <form action="./searchUser.html">
                <table>
                        <tr>
                                <td>Name</td>
                                <td><input type="text" size="30" value="${user.name}" name="name" id="name"></td>
                        </tr>
                        <tr>
                                <td>Surname</td>
                                <td><input type="text" value="${user.surname}" name="surname" id="surname"></td>
                        </tr>
                        <tr>
                                <td>Login</td>
                                <td><input type="text" value="${user.login}" name="login"></td>
                        </tr>
                        <tr>
                                <td>Country</td>
                                <td><input type="text" value="${user.country}" name="country"></td>
                        </tr>
                         <tr>
                                <td>Email</td>
                                <td><input type="text" value="${user.email}" name="email"></td>
                        </tr>
                </table>
                <br>
                <br /> <input type="submit" name="reg" value="Search" onClick="clearNullSelect()">
        </form>
        </section>
        </div>
        <br>
        <div id="content">	
        <table border="0" align="center" width="300">
        	<c:forEach items="${mapUsers}" var="user">
        	<tr>
            	    <td><h3><a href="./getPage.html?id=${user.key.id}">
                	<c:out value="${user.key.name}" />
                	<c:out value="${user.key.surname} "/>
                	</a></h3></td>
                	<td><h5><c:choose>
						<c:when test="${(user.key.id != loggedUser.id) and user.value}">					
							(Friend)						
						</c:when>
						<c:when test="${user.key.id == loggedUser.id}">
      					  	(Me)
    					</c:when>    					
    					<c:otherwise>
        					<div id="info${user.key.id}"> <input type="button" value="Add to Friends" onclick="doAjaxFriendRequestPost(${user.key.id})"><br/></div>
    					</c:otherwise>										
					</c:choose></h5></td>
			<tr>	
        	</c:forEach>
        	</table>
        </div>
</body>
</html>