<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <title>Comment System</title>
        <link rel="stylesheet" href="CSS/programdiary.css"/>
        <link rel="stylesheet" href="CSS/post.css"/>
        <script src="JScript/jquery-1.9.1.js"></script>      
        <script type="text/javascript">
        function doAjax() {
            $.ajax({
              url: './message.action',              
              data: {name : "${user.login}", description : $("#description").val(), ownerId : "${user.id}", receiverId : "${id}", flag : "1"},
              success: function(response) {
            	$('#comment_box').html(response);
              },
              error : function(e) {
                  alert('Error: ' + e);
              }
            });
          }
        </script>
    </head>
    <body>
<div id="content">
		<div class="im">        
    		<a href="./index.html">My Page (<b>${user.name}</b>)</a> &nbsp;<a href="<c:url value="/j_spring_security_logout" />" > Logout</a>			
		</div><br/>
		<div class="im">	
			<p>Page Owner : ${targetUser.name}</p>			  		
		    <a href="./getPage.html?id=${targetUser.id}"><br/>Title page</a><br/>		    
		</div>
  </div>  
<div id="container">
		<form>
			<table>			
				<tr>
					<td colspan="2">
						<h2>Message :</h2>
						<div>
							<textarea name="name" placeholder="Write your message..."
								id="description"></textarea>
								 <input type="hidden" name="id" value="${id}">
						</div>
					</td>
				</tr>
			</table>
			<div>
				<input type="submit" class="btn" onclick="doAjax()" value="Comment" id="comment_submit">
				
			</div>
		</form>
		
		<c:forEach items="${comments}" var="comment">
			<div class="comment_box" id="comment_box">
				<div class="image_box">
					<img src="${pageContext.request.contextPath}/${comment.ownerId.avatar}"
						align="left" width="50" height="50" alt="lorem" id="avatar_img" />
				</div>
				<div class='body'>

					<div>
						<span><a href="./post.html?id=${comment.ownerId.id}"><b>${comment.ownerId.name}</b></a><small>(${comment.tdate})</small></span> <span
							style="float: right;"><small> </small></span>
					</div>
					<div class="txt">${comment.description}</div>
					<div id="box"></div>
					<br>
					<c:choose>
						<c:when
							test="${comment.ownerId.id==user.id||comment.receiver.id==user.id}">
							<a
								href="./deleteCommentDialog.action?id=${comment.id}&usrId=${targetUser.id}">Delete</a>
							<br>
						</c:when>
					</c:choose>
				</div>
			</div>					
		 </c:forEach>
		
</div>

</body>
</html>
