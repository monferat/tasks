<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Comment System</title>
        <link rel="stylesheet" href="CSS/programdiary.css"/>
        <link rel="stylesheet" href="CSS/post.css"/>
        <link rel="stylesheet" type="text/css" href="CSS/form_style.css">
        <script src="JScript/jquery-1.9.1.js"></script>  
        <script type="text/javascript">
        function doAjax() {
            $.ajax({
              url: './message.action',
              data: {name : "${user.login}", description : $("#description").val(), ownerId : "${user.id}", receiverId : "${id}",flag : "0"},
              success: function(data) {
                $('#commet_box').html(data);
              },
              error : function(e,flag) {
                  alert('Error: ' + e + flag );
              }
            });
          }
        </script>
        <script type="text/javascript">
        function commentAjax(id) {
        	
        	var postId = id;
            $.ajax({
              url: './message.action',
              data: {name : "${user.login}", description : $("#descriptions"+postId).val(), ownerId : "${user.id}", receiverId : "${id}",flag : $("#flagId"+postId).val()},
              success: function(data) {
                $('#commet_box').html(data);
              },
              error : function(e,flag) {
                  alert('Error: ' + e + flag );
              }
            });
          }
        </script>
		<script>
			function anichange(objName) {
				if ($(objName).css('display') == 'none') {
					$(objName).animate({
						height : 'show'
					}, 400);
				} else {
					$(objName).animate({
						height : 'hide'
					}, 200);
				}
			}
		</script>
</head>
    <body>
<div id="content">
    <img class="im" src="${pageContext.request.contextPath}/${avatar}" align="left" height="255"  hspace="10" vspace="10" alt="lorem"/>    
    <p><a href="./index.html">My Page</a></p><br/>
     <p><a href="./chAvatar.html?id=${user.id}">Change Avatar</a></p><br/>
     <p><a href="./loadUser.html?id=${user.id}">Edit</a></p><br/>
	<p><a href="./listComments.html">Comments(${comments_size})</a></p><br/>		
	<h3>Username : ${user.name}</h3>
	
		<div class="im">	
			<p class="MyMargin">
			    <h3>Page Owner : ${targetUser.name}</h3> 
			</p> 	<br/>			
		    <br/><a href="./photo_album.html?id=${targetUser.id}">Photo Album</a><br/>
		    <br/>
			<a href="<c:url value="/j_spring_security_logout" />" > Logout</a><br/>
		    <a href="./getPage.html?id=${targetUser.id}"><br/>Title page</a><br/>
		    <a href="./getFriends.html?id=${targetUser.id}"><br/>Friends</a><br/>
		</div>
  </div>  
<div id="container">
		<form>
			<table>			
				<tr>
					<td colspan="2">
						<h2>Comment :</h2>
						<div>
							<textarea name="name" placeholder="Write your comment..."
								id="description"></textarea>
								 <input type="hidden" name="id" value="${id}">
						</div>
					</td>
				</tr>
			</table>
			<div>
				<input type="submit" class="btn" onclick="doAjax()" value="Comment" id="comment_submit">				
			</div>
		</form>
		<c:forEach items="${comments}" var="comment">
		<c:choose>
		  <c:when test="${comment.flag==0}">
			<div class="comment_box2" id="comment_box2">
				<div class="image_box">
					<img src="${pageContext.request.contextPath}/${comment.ownerId.avatar}"
						align="left" width="50" height="50" alt="lorem" id="avatar_img" />
				</div>
				<div class='body'>

					<div>
						<span><a href="./post.html?id=${comment.ownerId.id}"><b>${comment.ownerId.name}</b></a><small>(${comment.tdate})</small></span> <span
							style="float: right;"><small> </small></span>
					</div>
					<div class="txt">${comment.description}</div>
					<br>
					<c:choose>
						<c:when
							test="${comment.ownerId.id==user.id||comment.receiver.id==user.id}">
							<a href="./deleteComment.action?id=${comment.id}&usrId=${targetUser.id}">Delete</a>							
						</c:when>
					</c:choose>
				</div>
				<input type="button" value="comments" onclick="anichange('.divId${comment.id}'); return false"/>	
				<div id="container" class="divId${comment.id}" style="display: none">
					<form>
						<table>			
							<tr>
								<td colspan="2">
									<h5>Comment :</h5>
									<div>
										<textarea name="name" placeholder="Write your comment..."
											id="descriptions${comment.id}"  style="width: 350px; height: 50px;"></textarea>
											 <input type="hidden" name="id" value="${id}">
									</div>
								</td>
							</tr>
						</table>
						<div>
							<input type="submit" onclick="commentAjax(${comment.id})" value="Comment" id="comment_submit${comment.id}">
							<input type="hidden" id="flagId${comment.id}" value="${comment.id}">
						</div>
					</form>
					<c:forEach items="${comments}" var="smallcomment">
					<c:choose>
					  <c:when test="${smallcomment.flag==comment.id}">
						<div class="comment_box" id="comment_box">
							<div class="image_box">
								<img src="${pageContext.request.contextPath}/${smallcomment.ownerId.avatar}"
									align="left" width="25" height="25" alt="lorem" id="avatar_img" />
							</div>
							<div class='body'>
			
								<div>
									<span><a href="./post.html?id=${smallcomment.ownerId.id}"><b>${smallcomment.ownerId.name}</b></a><small>(${smallcomment.tdate})</small></span> <span
										style="float: right;"><small> </small></span>
								</div>
								<div class="txt">${smallcomment.description}</div>								
								<c:choose>
									<c:when
										test="${smallcomment.ownerId.id==user.id||smallcomment.receiver.id==user.id}">
										<a href="./deleteComment.action?id=${smallcomment.id}&usrId=${targetUser.id}">Delete</a>										
									</c:when>
								</c:choose>
							</div>
						</div>
						 </c:when>
						</c:choose>
					 </c:forEach>
				</div>
			</div>
			 </c:when>
			</c:choose>
		 </c:forEach>
</div>

</body>
</html>
