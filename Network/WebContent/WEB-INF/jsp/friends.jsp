<%@ page contentType="text/html; charset=windows-1251"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="JScript/jquery-1.9.1.js"></script>
<script>
	function anichange(objName) {
		if ($(objName).css('display') == 'none') {
			$(objName).animate({ height : 'show'}, 400);
		} else {	
			$(objName).animate({ height : 'hide'}, 200);
		}
	}
</script>
<title>Edit info</title>
 <link rel="stylesheet" type="text/css" href="CSS/form_style.css">
</head>

<body>
	<a href="./index.html">My Page</a>

    <p>${targetUser.name}'s Friends</p>
    <div class="CSSTable">
	<table border="2">
		<c:forEach items="${myFriends}" var="friend">
			<tr>
				<td><a href="./getPage.html?id=${friend.id}"> <c:out
							value="${friend.name}" /> <c:out value="${friend.surname}:" />
				</a></td>
			<c:choose>
				<c:when test="${targetUser.id == loggedUser.id}">										
					<td><a href="./deleteFriend.html?userId1=${friend.id}&userId2=${loggedUser.id}">Delete</a></td>
					<td><a href="./messages.html?id=${friend.id}">Send Message</a></td>	
				</c:when>
			</c:choose>				
			</tr>
		</c:forEach>
	</table>
	</div>
		<c:choose>
			<c:when test="${targetUser.id == loggedUser.id}">				
				<input type="button" value="Friend Requests(${requestCount})" onclick="anichange('#divId'); return false"/>				
				<div id="divId" style="display: none">
					<table border="1" class="CSSTable">
					    <c:forEach items="${friendsRequest}" var="fr">
					        <tr>
					            <td><a href="./getPage.html?id=${fr.id}"> <c:out value="${fr.name}" /> <c:out value="${fr.surname}:" />
								</a></td>			       	            	            
					            <td><a href="./addFriend.html?userId=${fr.id}">Confirm</a></td>
					            <td><a href="./deleteFriend.html?userId1=${fr.id}&userId2=${loggedUser.id}">Ignore</a></td>					         
					        </tr>
					    </c:forEach>
					</table>
				</div>
		</c:when>
		</c:choose>
</body>
</html>