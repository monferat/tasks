<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
	<title>CSS3 Form</title> 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
		<link rel="stylesheet" type="text/css" href="CSS/user_style.css">
		<script src="JScript/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
		<script>
			$(document).ready(function(){
				$("#form").validate({
					rules:{
						title: {
							required: true,
							minlength: 4
						},
						description: {
							required: true,
							minlength: 6
						}
					},
				    messages: {
				    	title: {
				    		required: "Title required",
				    		minlength: "Your title must be 4 characters at least"
				   		},				   		
				   		description: {
				   			required: "Description required",
				   			minlength: "Description must be 6 characters at least"
				   		}
					}
				});
			});
		</script>
	</head>
	<body>
		<form class="form" id="form" action="./saveComment.html">
			<div><h1>Create comment:</h1><br></div>
		    Title<br> <input type="text" value="${comment.title}" name="title"><br>
		    Description<br> <input type="text" value="${comment.description}" name="description"><br>	
		    OwnerId
		    <select name="owner.id">
		    <c:forEach items="${users}" var="user">
		        <option value="${user.id}">${user.name} (${user.id})</option>
		    </c:forEach>
		    </select><br>
		    <input type="hidden" name="id" value="${comment.id}">
		    <p class="submit">  
				<input type="submit" value="Save" />  
		  	</p>  
		</form>
	</body>
</html>