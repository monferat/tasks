<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
	<head>
	<title>CSS3 Form</title> 
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> 
		<link rel="stylesheet" type="text/css" href="CSS/user_style.css">
		<script src="JScript/jquery-1.9.1.js"></script>
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
		<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
		<script src="JScript/CheckLogin.js"></script>
		<script src="JScript/Validation.js"></script>
		<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		<link rel="stylesheet" href="/resources/demos/style.css" />
		<link rel="stylesheet" href="CSS/programdiary.css"/>
		<script>
        $(function() {
                $("#datepicker").datepicker({
                        changeMonth : true,
                        changeYear : true,
                        minDate : "01/01/1910",
                        maxDate : "12/31/2007",
                        yearRange : '-100:+10'
                });
        });
		</script>
		
	</head>
	<body>
		<form class="form" id="form" action="./saveUser.html">
			<div><h1>Registration</h1></div>			
			<div><h1>Edit: </h1><br></div>						
			<p class="login"> <label for="login">Login</label></p>
			<p class="login"> <input type="text" value="${user.login}" name="login" id="login" /></p>
			<p class="login"><span class="password">
		    <label for="password3">Password</label>
			</span></p>  
			<p class="password">  
				<input type="text" value="${user.password}" name="password" id="password" />
		    </p>
			<p class="password"><span class="name">
			  <label for="name3">Name</label>
            </span> </p>  
			<p class="name">  
				<input type="text" value="${user.name}" name="name" id="name" />
		  	</p>
		  	<p class="login"> <label for="login">Surname</label></p>	  
		  	<p class="login"><input type="text" value="${user.surname}" name="surname"/></p>
		  	<p class="login"> <label for="login">Date</label></p>
		  	<p><input type="text" id="datepicker" name="date"/></p>
		  	<p class="login"> <label for="login">Country</label></p>	  
		  	<p class="login"><input type="text" value="${user.country}" name="country"/></p>
		  	<p class="login"> <label for="login">Email</label></p>	  
		  	<p class="login"><input type="text" value="${user.email}" name="email"/></p>	   
		    <input type="hidden" name="id" value="${user.id}">
		    <input type="hidden" name="enabled" value="${user.enabled}">
		    <input type="hidden" name="authority" value="${user.authority}">
		    <input type="hidden" name="avatar" value="${user.avatar}">

		<p class="submit">  
				<input type="submit" value="Save" id="submit" />  
		  	</p>  
		</form>
	</body>	
</html>