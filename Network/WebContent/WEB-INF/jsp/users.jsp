<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<title>CSS3 Form Table</title> 
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="CSS/form_style.css">
	</head>
<body>
	<div class="CSSTable" >
		<table border="1">
		    <tr>
		        <th>Login</th>
		        <th>Password</th>
		        <th>Name</th>
		        <th></th>
		        <th></th>
		    </tr>
		
		<c:forEach items="${users}" var="user">
		    <tr>
		        <td>${user.login}</td>
		        <td>${user.password}</td>
		        <td>${user.name}</td>
		        <td><a href="./loadUser.html?id=${user.id}">Edit</a></td>
		        <td style="color: red;"><a href="./deleteUser.html?id=${user.id}">Delete</a></td>
		
		    </tr>
		</c:forEach>
		</table>
	</div>
	<p><a href="./loadUser.html">Create New User</a></p>
	<p><a href="./index.html">Main Page</a></p>
</body>
</html>
