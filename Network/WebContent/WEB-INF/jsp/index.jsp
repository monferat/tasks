<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="CSS/idx_style.css"> 
<title>Insert title here</title>
</head>
<body>
<div id="main">
  
	<img src="${pageContext.request.contextPath}/${avatar}" align="left" width="255" height="255"  hspace="10" vspace="10" alt="lorem"/>
	 <p><a href="./index.html">My Page</a></p>
	 <p><a href="./chAvatar.html?id=${loggedUser.id}">Change Avatar</a></p>
	 <p><a href="./loadUser.html?id=${loggedUser.id}">Edit</a></p>
	 <p><a href="./searchUser.html">Search</a></p>
	 <a href="./getMyMessages.html?id=${loggedUser.id}">My Messages</a>
    <input type="hidden" name="id" value="${user.id}">		
	
	<h3>Username : ${loggedUser.name}</h3><br/><br/>
	<br/>	<br/>
	<p class="MyMargin">
	    <h3>Page Owner : ${targetUser.name}&nbsp;${targetUser.surname}</h3> 
	</p> 	
	
	<a href="<c:url value="/j_spring_security_logout" />" > Logout</a>	
    <a href="./post.html?id=${targetUser.id}"><br/>Post</a><br/>    
    <a href="./photo_album.html?id=${targetUser.id}"><br/>Photo Album</a>
    <a href="./getFriends.html?id=${targetUser.id}"><br/>Friends</a><br/>    
    
		<div>
			<c:choose>
				<c:when test="${(targetUser.id != loggedUser.id) and !check}">
					<br>
					<a href="./addFriend.html?userId=${targetUser.id}">Add to friends</a>					
				</c:when>				
			</c:choose>
			<c:choose>
				<c:when test="${targetUser.id != loggedUser.id}">
					<br>
					<a href="./messages.html?id=${targetUser.id}">Send Message</a>					
				</c:when>
			</c:choose>		
		</div>
	</div>   
</body>
</html>
